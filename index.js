const express = require('express');
const cookieParser = require('cookie-parser');
const cors = require('cors');
const routes = require('./app/routes/index');
const config = require('./config');
const { Sequelize } = require('sequelize');
const logger = require('morgan');
// const session = require('express-session');
// const flash = require('express-flash');
var path = require('path') 
var fileUpload = require('express-fileupload');

const app = express();
appInit(app);

app.listen(config.PORT, () => {
    console.log(`Listening on port 3000..`);
    const db = new Sequelize(
      config.DB_NAME,
      config.DB_USERNAME,
      config.DB_PASSWORD,
      {
        host: config.DB_HOST,
        port: config.DB_PORT,
        dialect: config.DB_DIALECT,
        logging: false
      }
    );
    db.authenticate()
    .then(() => {
        console.log('Successfully connected to Local database');
    })
    .catch(err => {
        console.error('Failed to connect into Local database:', err);
        process.exit();
    });
});

function appInit(app){
    app.use(logger('dev'));
    app.use(cookieParser('secret'));
    app.use('/static', express.static('app/static'));
    app.use(cors());
    app.use(express.json());
    app.use(fileUpload());
    app.use(express.urlencoded({ extended: true }));
    app.set('views', path.join(__dirname, './app/view'))
    app.set('view engine' , 'ejs');
    app.use('/', routes);
    process.env.TZ = 'Asia/Jakarta';
}