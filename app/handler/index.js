const user = require('./user')
const register_user = require('./register_user')
const login = require('./login')
const BeritaTerkini = require('./beritaTerkini')
const detail_berita = require('./detail_berita')
const admin = require('./admin')
const auth = require('./login')
const edit_profile_user = require('./edit_profile_user')
const edit_password_user = require('./edit_password_user')
const admin_komentar = require('./admin_komentar')
const kategori_berita = require('./kategori_berita')
const admin_berita = require('./admin-berita')
const hashtag_berita = require('./hashtag_berita')
const homepage = require('./homepage')
const watchlist = require('./watchlist')
const aboutUs = require('./about_us')
const sitemap = require('./sitemap')


module.exports = {
  user,
  register_user,
  login,
  admin,
  detail_berita,
  auth,
  BeritaTerkini,
  edit_profile_user,
  edit_password_user,
  admin_komentar,
  kategori_berita,
  admin_berita,
  hashtag_berita,
  homepage,
  watchlist,
  aboutUs,
  sitemap
}
