const { berita } = require('../model/berita')
const { kategori } = require('../model/kategori')
const { hashtag } = require('../model/hashtag')
const { hashtag_berita } = require('../model/hashtag_berita')
const { Pagination } = require('../utils/helper')
const jwt = require('jsonwebtoken')

const showHashtag = async (req, res) => {
    const tokenCookies = req.cookies.token
      let decryptedToken = null
      if (tokenCookies) {
          // cek token 
          decryptedToken = jwt.verify(tokenCookies, 'hahaha')
      }
    const listHashtag = await hashtag.findAll()
    const listBerita = await berita.findAll()

    const nama_tag = req.params.nama_tag
    const tag = await hashtag.findOne({
        where: {
         nama_tag: nama_tag
        }
    })

    const data = Pagination(listBerita,req.query.page,5)
    
    res.status(200).render('hashtag',{
        listHashtag,
        data,
        nama_tag,
        user:decryptedToken
    })
}

module.exports = {
    showHashtag
}