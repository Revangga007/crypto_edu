const { berita } = require('../model/berita')
const { hashtag } = require('../model/hashtag')
const { hashtag_berita } = require('../model/hashtag_berita')
const jwt = require('jsonwebtoken')

const viewAllBerita = async (req, res) => {

    try { 
        const tokenCookies = req.cookies.token
        let decryptedToken = null
        if (tokenCookies) {
            // cek token 
            decryptedToken = jwt.verify(tokenCookies, 'hahaha')
        }

        let dataMaster = []
        const beritaItem = await berita.findAll({ limit: 8,  order:[['id', 'ASC']] })
        const carousel = await berita.findAll({ limit: 3, order:[['createdAt', 'DESC']] })
        const hashtagItem = await hashtag.findAll()  
        const hb =  await hashtag_berita.findAll()

        for (let i = 0; i < beritaItem.length; i++) {
            beritaItem[i].dataValues.hashtag = []
           for (let j = 0; j < hb.length; j++) {
             let tags = hashtagItem.find(tag => tag.id == hb[j].id_hashtag && hb[j].id_berita == beritaItem[i].dataValues.id)
             if (tags) {
                 beritaItem[i].dataValues.hashtag.push(tags)
             }
           }
           dataMaster.push(beritaItem[i])
        }

        res.status(200).render('homepage', {
            datas:dataMaster,
            carousel,
            user: decryptedToken,
        })
        // res.status(200).send(decryptedToken)
    
    } catch (error) {
        console.log(error)
        res.status(500).json({
            message:'ini error',
            error
        })
    }
    


    // res.status(200).render('homepage')

}

module.exports ={
    viewAllBerita
}




// delsoon 

  // const token = req.get("authorization") || null
        // let userLogin = null
        // if(token !== null) {
        //     userLogin = await user.findOne({
        //     where: {
        //         id: token
        //     }
        //     })
        //     res.set('authorization', userLogin.id);
        // }