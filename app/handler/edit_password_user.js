const bcryptjs = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { user } = require('../model/user')

const showPasswordUser = (req, res) => {
    try {
        const tokenCookies = req.cookies.token
        let decryptedToken = null

        if (tokenCookies) {
            // cek token 
            decryptedToken = jwt.verify(tokenCookies, 'hahaha')
        }
        res.status(200).render('edit_password_user', {
            username: req.params.username,
            user:decryptedToken
        })
    } catch (err) {
        res.status(400).json(err)
    }
  }

const updatePasswordUser = (req, res) => {
    const username = req.params.username
    user.findOne({
        where: {username: username}
    })
    .then((data) => {
        if(bcryptjs.compareSync(req.body.password_lama, data.password)) {
            if(req.body.password_baru == req.body.konfirmasi_password) {
                user.update({
                    password: bcryptjs.hashSync(req.body.password_baru, 10)
                }, { 
                    where: {username: username}
                })
                .then(() =>{
                    res.status(200).redirect(`/user/edit-profile/${username}`)
                })
                .catch(() => {
                    res.status(400).redirect(`/user/edit-password/${username}`)
                })

            } else {
                console.log("konfirmasi password tidak sesuai")
                res.status(400).redirect(`/user/edit-password/${username}`)
            }
        } else{
            console.log("password lama tidak sesuai")
            res.status(400).redirect(`/user/edit-password/${username}`)
        }
    })
    .catch((err) => {
        console.log("Gagal ubah password")
        res.status(400).redirect(`/user/edit-password/${username}`)
    })
}

module.exports = {showPasswordUser, updatePasswordUser}