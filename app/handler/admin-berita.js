const {
    berita
} = require('../model/berita')
const {
    kategori
} = require('../model/kategori')

const {
    hashtag
} = require('../model/hashtag')

const {
    hashtag_berita
} = require('../model/hashtag_berita')

const showCreateBerita = async function (req, res) {
    const tag = await hashtag.findAll({})
    const category = await kategori.findAll({})
    res.status(200).render('admin_form_berita', {
        tag: tag,
        category: category
    })
}

const create = async function (req, res) {
    let judul = req.body.judul
    let subJudul = req.body.subJudul
    let category = req.body.kategori
    let isiBerita = req.body.isiBerita
    let thumb_judul = isiBerita.substring(0, isiBerita.indexOf('\n'))
    let id_hashtag = req.body.hashtag
    let draft = false

    function isDraft() {
        draft = true
    }

    let slug = slugify(judul)

    function slugify(text) {
        return text.toString().toLowerCase().replace(/\s+/g, '-') // Ganti spasi dengan -
            .replace(/[^\w\-]+/g, '') // Hapus semua karakter non-word
            .replace(/\-\-+/g, '-') // Ganti multiple - atau single -
            .replace(/^-+/, '')
            .replace(/-+$/, '');
    }

    if (!req.files)
        return res.status(400).send('No files were uploaded.');

    var file = req.files.uploaded_image;
    var img_name = file.name;

    console.log(req.body)
    const data = await berita.create({
        judul: judul,
        subjudul: subJudul,
        thumbnail: img_name,
        isiberita: isiBerita,
        id_kategori_berita: category,
        thumb_judul: thumb_judul,
        slug: slug,
        // id_hashtag: id_hashtag,
        isDraft: draft
    })

    const hashtagberita = await hashtag_berita.create({
        id_berita: data.id,
        id_hashtag: id_hashtag
    })

    if (file.mimetype == "image/jpeg" || file.mimetype == "image/png" || file.mimetype == "image/gif") {

        file.mv('app/static/image/' + file.name, function (err) {

            if (err){
                return res.status(500).send(err);
            }else{
                // res.status(200).json({
                //     message: "data berhasil diinsert",
                //     data: data,
                //     tag:hashtagberita
                // }).end()
                // return
                res.redirect('/admin/showBerita')
            }
                
        });
    } else {
        message = "This format is not allowed , please upload file with '.png','.gif','.jpg'";
        res.render('admin_form_berita', {
            message: message
        });
    }
}

const showBerita = async function (req, res) {
    let isDraft;
    const news = await berita.findAll()
    const resp = []
    for (val of news) {
        const cat = await kategori.findOne({
            where: {
                id: val.id_kategori_berita
            }
        })
        if (val.isDraft == 1) {
            isDraft = "Draft"
        } else {
            isDraft = "Aktif"
        }
        resp.push({
            kategori: cat.nama_kategori,
            id: val.id,
            judul: val.judul,
            subjudul: val.subjudul,
            isiberita: val.isiberita,
            // id_kategori_berita: val.id_kategori_berita,
            // slug: val.slug,
            // thumbnail: val.thumbnail,
            // thumb_judul: val.thumb_judul,
            isDraft: isDraft
        })
    }

    res.status(200).render('admin_tabel_berita', {
        data: resp,
    })
    return
}

const hapus = async function (req, res) {
    try {
        let id = req.params.id
        
        const deleteHashtag = await hashtag_berita.destroy({
            where:{
                id_berita: id
            }
        })
        const data = await berita.destroy({
            where: {
                id: id
            }
        })
         res.status(200).redirect('/admin/showBerita')
        return
    } catch (error) {
        console.log(error)
        res.status(400).json({
            alert: "data gagal dihapus",
            data: null
        }).end()
        return
    }
    

    // if (!data) {
    //     res.status(400).json({
    //         alert: "data gagal dihapus",
    //         data: null
    //     }).end()
    //     return
    // }

    // res.status(200).json({
    //     alert: "data berhasil dihapus",
    //     data: data,
    // }).end()
    // return

}

const showUpdate = async function (req, res) {
    let id = req.params.id
    const tag = await hashtag.findAll({})
    const category = await kategori.findAll({})
    const news = await berita.findOne({
        where: {
            id: id
        }
    })

    res.status(200).render('admin_update_berita', {
        tag: tag,
        data: news,
        category: category
    })
}

const update = async function (req, res) {
    let id = req.params.id
    let judul = req.body.judul
    let subJudul = req.body.subJudul
    let category = req.body.kategori
    let isiBerita = req.body.isiBerita
    let thumb_judul = isiBerita.substring(0, isiBerita.indexOf('\n'))
    let id_hashtag = req.body.hashtag
    let draft = false

    function isDraft() {
        draft = true
    }

    let slug = slugify(judul)

    function slugify(text) {
        return text.toString().toLowerCase().replace(/\s+/g, '-') // Ganti spasi dengan -
            .replace(/[^\w\-]+/g, '') // Hapus semua karakter non-word
            .replace(/\-\-+/g, '-') // Ganti multiple - atau single -
            .replace(/^-+/, '')
            .replace(/-+$/, '');
    }

    if (!req.files)
        return res.status(400).send('No files were uploaded.');

    var file = req.files.uploaded_image;
    var img_name = file.name;

    // console.log(req.body)
    const data = await berita.update({
        judul: judul,
        subjudul: subJudul,
        thumbnail: img_name,
        isiberita: isiBerita,
        id_kategori_berita: category,
        thumb_judul: thumb_judul,
        slug: slug,
        // id_hashtag: id_hashtag,
        isDraft: draft
    }, {
        where: {
            id: id
        }
    })
    const hashtagberita = await hashtag_berita.update({
        id_hashtag: id_hashtag
    }, {
        where: {
            id_berita: id
        }
    })

    if (!data) {
        res.status(400).json({
            message: "data gagal diupdate",
            data: null
        }).end()
        return
    }

    if (file.mimetype == "image/jpeg" || file.mimetype == "image/png" || file.mimetype == "image/gif") {

        file.mv('app/static/image/' + file.name, function (err) {

            if (err){
                return res.status(500).send(err);
            }else{
                res.redirect('/admin/showBerita')
            }
            // res.status(200).json({
            //     message: "data berhasil diupdate",
            //     // tag: tag,
            //     data: data,
            //     tag: hashtagberita
            // }).end()
            // return
        });
    } else {
        message = "This format is not allowed , please upload file with '.png','.gif','.jpg'";
        res.render('admin_update_berita', {
            message: message
        });
    }
}

module.exports = {
    showBerita,
    showCreateBerita,
    create,
    hapus,
    showUpdate,
    update
}