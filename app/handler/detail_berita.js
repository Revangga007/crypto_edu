const { berita } = require('../model/berita')
const { komentar } = require('../model/komentar')
const { komentar_berita } = require('../model/komentar_berita')
const Sequelize = require('sequelize');
const { user } = require('../model/user');
const moment = require('moment');
const Op = Sequelize.Op;
const jwt = require('jsonwebtoken')

const { hashtag } = require('../model/hashtag') //import tabel hashtag
const { hashtag_berita } = require('../model/hashtag_berita') //import tabel hashtag_berita

const showDetail = async function(req, res) {


    const slug = req.params.slug
    const tokenCookies = req.cookies.token
    let decryptedToken = null
    let idKomentator = 0;
    if (tokenCookies) {
        // cek token 
        decryptedToken = jwt.verify(tokenCookies, 'hahaha')
        idKomentator = decryptedToken.id
    }
    const detail = await berita.findOne({
        where: {
            slug: slug
        }
    })

    const terkait = await berita.findAll({
        where: {
            id_kategori_berita: detail.id_kategori_berita,
            id: {
                [Op.ne]: detail.id
            }
        },
        order: [
            ['createdAt', 'DESC']
        ],
        limit: 4
    })

    const kmBerita = await komentar_berita.findAll({
        where: {
            id_berita: detail.id
        }
    })

    const kmn = []
    for (const val of kmBerita) {
        const temp = await komentar.findOne({
            where: {
                id: val.id_komentar
            }
        })
        const uname = await user.findOne({
            where: {
                id: temp.id_user
            }
        })

        const convDate = moment(temp.createdAt).format('D MMMM YYYY');

        kmn.push({
            name: uname.nama_lengkap,
            date: convDate,
            comment: temp.isiKomentar
        })
    }
    

    //Ngambil id_hashtag per berita
    const idBerita = await hashtag_berita.findAll({
        where: {
            id_berita: detail.id
        }
    })

    //Ngambil nama hashtag
    const nama_hashtag = []
    for (let index = 0; index < idBerita.length; index++) {
        const namaHashtag = await hashtag.findOne({
            where: {
                id: idBerita[index].id_hashtag
            }
        })
        nama_hashtag.push({
            nama_tag: namaHashtag.nama_tag
        })
    }

    if (!detail) {
        res.status(404).json({
            message: `Data tidak ditemukan`,
            data: null
        }).end()
        return
    }
    res.status(200).render('detail_berita', {
        message: `Berhasil`,
        data: detail,
        dataterkait: terkait,
        isikomentar: kmn,
        hashtagberita: nama_hashtag, //masukin nama hashtag ke variabel,
        user: decryptedToken,
        idKomentator
    })
    return
    // res.status(200).send({kmBerita})
}

const submitKomen = async function(req, res) {
    let isi = req.body.isi
    let url = req.body.url
    let findid = req.body.user
    const lastSegment = url.split("/").pop();


    const komen = await komentar.create({
        isiKomentar: isi,
        id_user: findid,
        isReported: 0
    })

    const findbyslug = await berita.findOne({
        where: {
            slug: lastSegment
        }
    })

    const komenGet = await komentar_berita.create({
        id_berita: findbyslug.id,
        id_komentar: komen.id
    })

    if (!komen && !komenGet) {
        res.status(404).json({
            message: `Komentar gagal di insert`,
            data: null
        }).end()
        return
    }
    res.status(200).json({
        message: `Komentar berhasil di insert`,
        data: komen
    })
    return
}

module.exports = {
    showDetail,
    submitKomen
}