const { komentar } = require('../model/komentar')
const { user } = require('../model/user');
const { komentar_berita } = require('../model/komentar_berita')
const moment = require('moment');

const showKomentar = async function(req, res) {
    const show = await komentar.findAll({})

    const row = []
    for (const val of show) {
        const temp = await komentar.findOne({
            where: {
                id: val.id
            }
        })
        const uname = await user.findOne({
            where: {
                id: temp.id_user
            }
        })

        const convDate = moment(temp.createdAt).format('MMMM Do YYYY, h:mm:ss a');

        row.push({
            id: temp.id,
            username: uname.username,
            isi: temp.isiKomentar,
            waktu: convDate
        })
    }



    if (!show) {
        res.status(404).json({
            message: `Data tidak ditemukan`,
            data: null
        }).end()
        return
    }
    res.status(200).render('admin_komentar', {
        message: `Berhasil`,
        data: row
    })
    return
}

const deleteKomentar = async function(req, res) {
    let id = req.params.id

    const find = await komentar.findOne({
        where: {
            id: id
        }
    })

    const data = await komentar.destroy({
        where: {
            id: find.id
        }
    })

    const delkom = await komentar_berita.destroy({
        where: {
            id_komentar: find.id
        }
    })

    if (!data && !delkom) {
        res.status(400).json({
            message: "data gagal dihapus",
            data: null
        }).end()
        return
    }
    res.redirect('/admin/komentar')
    // res.status(200).json({
    //     message: "data berhasil dihapus",
    //     data: data,
    // }).end()
    // return
}

module.exports = {
    showKomentar,
    deleteKomentar
}