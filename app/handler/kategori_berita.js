const { berita } = require('../model/berita')
const { kategori } = require('../model/kategori')
const { hashtag } = require('../model/hashtag')
const { hashtag_berita } = require('../model/hashtag_berita')
const { Pagination } = require('../utils/helper')
const jwt = require('jsonwebtoken')

const showBerita = async function(req, res) {

  // add auth 
  // const token = req.get("authorization") || null
  // let userLogin = null
  // if(token !== null) {
  //   userLogin = await user.findOne({
  //     where: {
  //       id: token
  //     }
  //   })
  //   res.set('authorization', userLogin.id);
  // }
  // add auth end


    const tokenCookies = req.cookies.token
    let decryptedToken = null
    if (tokenCookies) {
        // cek token 
        decryptedToken = jwt.verify(tokenCookies, 'hahaha')
    }

    const news = await berita.findAll({
      order: [
        ['createdAt', 'DESC']
      ]
    })
    const findHashtagBerita = await hashtag_berita.findAll()

    const nama_kategori = req.params.nama_kategori.toLowerCase()
    const resp = []
    const hash = []
    const nama = []
    
    const cat = await kategori.findOne({
        where: {
          nama_kategori: nama_kategori
        }
      })

    for(val of news) {
      //nangkep kategori > nama_kategori
      if(val.id_kategori_berita == cat.id){
        let p = ""
        for(var j = 0; j < 170; j++) {
          p = p + val.isiberita[j]
        }
        resp.push({
          kategori: cat.nama_kategori,
          id: val.id,
          judul: val.judul,
          subjudul: val.subjudul,
          isiberita: val.isiberita,
          id_kategori_berita: val.id_kategori_berita,
          slug: val.slug,
          thumbnail: val.thumbnail,
          thumb_judul: p,
          isDraft: val.isDraft,
          createdAt: val.createdAt
        })
      }

      // nangkep hashtag_berita > id_hashtag
      let p = ""
      for(var i = 0; i < findHashtagBerita.length; i++){
        if(val.id == findHashtagBerita[i].id_berita){
          p = p + findHashtagBerita[i].id_hashtag
        }
      }
      hash.push({
        id_hashtag: p,
        id_berita: val.id
      })
    }
    
    // nangkep hashtag > nama_tag
    for (var k = 0; k < hash.length; k++) {
      for (let index = 0; index < hash[k].id_hashtag.length; index++) {
        const simpan_nama_tag = await hashtag.findOne({
          where: {
            id: hash[k].id_hashtag[index]
          }
        })
        nama.push({
          id: simpan_nama_tag.id,
          id_berita: hash[k].id_berita,
          nama_tag: simpan_nama_tag.nama_tag
        })
      }
    }
    
    // ngilangin duplikasi hashtag
    for (var i = 0; i < nama.length; i++) {
      for (var j = 0; j < nama.length; j++) {
        if(i !== j){
          if(nama[i].nama_tag === nama[j].nama_tag){
            nama.splice(j, 1)
          }
        }
      }
    }

    dataPage = Pagination(resp, req.query.page, 5)
    res.status(200).render('kategori', {
      data: resp,
      nama: cat,
      namaHashtag: nama,
      page: dataPage,
      user:decryptedToken
    })
    return
  
    // res.status(200).send(cat)
}

module.exports = {
    showBerita
}