const bcryptjs = require('bcryptjs')
const { user } = require('../model/user')


const userLogin = null
const showRegister = async(req, res) => {
    res.status(200).render('register_user', {
        user:userLogin
    })
}

const register = (req, res) => {
    user.create({
        email: req.body.email,
        password: bcryptjs.hashSync(req.body.password, 10),
        nama_lengkap: req.body.nama_lengkap,
        nomor_telepon: req.body.nomor_telepon,
        username: req.body.username,
        tgl_lahir: req.body.tgl_lahir,
        gender: req.body.gender,
        isBlocked: false
    })
    .then((result) => {
        res.status(201).redirect('../user/login')
    })
    .catch((err) => {
        res.status(400).redirect('../user/register')
    })
}

module.exports = {showRegister, register}