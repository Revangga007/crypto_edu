const jwt = require('jsonwebtoken')

const getSitemap = async (req, res) =>{
  const tokenCookies = req.cookies.token
  let decryptedToken = null
  if (tokenCookies) {
      // cek token 
      decryptedToken = jwt.verify(tokenCookies, 'hahaha')
  }
  res.status(200).render('sitemap', {
      user:decryptedToken
  })

}

module.exports = {
    getSitemap
}