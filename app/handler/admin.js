const bcryptjs = require('bcryptjs')
const { admin } = require('../model/admin')
const {user} = require('../model/user')
const { hashtag } = require('../model/hashtag')
const { kategori } = require('../model/kategori')

const adminPage = async(req, res) => {
    res.status(200).render('admin')
}

const showLoginAdmin = async(req, res) => {
    res.status(200).render('loginAdmin')
}

const addAdmin = (req,res) => {
    admin.create({
        email: req.body.email,
        password: bcryptjs.hashSync(req.body.password, 10),
        role: req.body.role,
        isBlocked: false
    })
    .then(admin => {
        res.status(201).json(admin)
    })
    .catch(err => {
        res.status(422).json("Can't add admin")
    })
}

const loginAdmin = async (req, res) => {
    const body = req.body;

    //mencari email yang diinput di dalam database
    const user = await admin.findOne({where: {email: body.email} });

    //jika email tidak ditemukan
    if (user === null) {
        res.redirect('/admin/login');
    }
    //jika email ditemukan
    else {
        const validPassword = await bcryptjs.compare(body.password, user.password);
        if (validPassword) {
            res.redirect('/admin');
        }
        else {
            res.redirect('/admin/login');
        }
    }

}

const getDataBlocking = async (req, res) =>{
    
    const getActive = await user.findAll({
        where:{
            isBlocked:false
        }
    })
    const getBlocked = await user.findAll({
        where:{
            isBlocked:true
        }
    })

    const totalAktif = await user.count({
            where:{
                isBlocked:false
            },
            distinct:true
    })
    const totalBlocked = await user.count({
            where:{
                isBlocked:true
            },
            distinct:true
    })
    // console.log(totalAktif)

    res.status(200).render('admin/block-users', {
        dataActive:getActive,
        dataBlocked:getBlocked,
        totalAktif,
        totalBlocked
    })
}

const blockingUser = async (req,res) =>{
    const {id, isBlocked} = req.body
    try {
        await user.update({
            isBlocked
        },{
            where: {
                id
            }
        })
        return res.json({
            message:"berhasil dirubah",
            error:"",
            
        })
    } catch (error) {
      return  res.status(500).send(error)
    }
}

//CRUD KATEGORI =================================================================================

const showAllKategori  = (req,res) => {
    kategori.findAll({
        order: [
            ['id', 'ASC'],
        ],
    })
    .then(allKategori => {
        res.render('kategori_admin', {allKategori});
    }) .catch(err => {
        console.log(err)
    }) 
}

const createKategori = (req,res) => {
    const {nama_kategori} = req.body
    kategori.create({
        nama_kategori
    })
    .then(kategori => {
        res.redirect('/admin/kategori')
    })
    .catch(err => {
        res.status("Kategori gagal dibuat.")
    })
}

const formUpdateKategori = async(req, res) => {
    res.status(200).render('kategori_admin_ubah')
}

const editKategori = (req,res) => {
    const {nama_kategori} = req.body
    kategori.update({
        nama_kategori
    }, {
        where: {id:req.params.id}
    })
    .then(kategori => {
        res.redirect('/admin/kategori')
    }) .catch(err => {
        res.status("Kategori gagal diubah.")
    })
}

const deleteKategori = (req,res) => {
    kategori.destroy({
        where: {id:req.params.id}
    })
    .then(kategori => {
        res.redirect('/admin/kategori')
    }) .catch(err => {
        res.status("Kategori gagal dihapus.")
    })
}

//CRUD HASHTAG ==================================================================================

const showAllHashtag  = (req,res) => {
    hashtag.findAll({
        order: [
            ['id', 'ASC'],
        ],
    })
    .then(allHashtag => {
        res.render('hashtag_admin', {allHashtag});
    }) .catch(err => {
        console.log(err)
    }) 
}

const createHashtag = (req,res) => {
    const {nama_tag} = req.body
    hashtag.create({
        nama_tag
    })
    .then(kategori => {
        res.redirect('/admin/hashtag')
    })
    .catch(err => {
        res.status("Hashtag gagal dibuat.")
    })
}

const formUpdateHashtag = async(req, res) => {
    res.status(200).render('hashtag_admin_ubah')
}

const editHashtag = (req,res) => {
    const {nama_tag} = req.body
    hashtag.update({
        nama_tag
    }, {
        where: {id:req.params.id}
    })
    .then(hashtag => {
        res.redirect('/admin/hashtag')
    }) .catch(err => {
        res.status("Hashtag gagal diubah.")
    })
    
}

const deleteHashtag = (req,res) => {
    hashtag.destroy({
        where: {id:req.params.id}
    })
    .then(hashtag => {
        res.redirect('/admin/hashtag')
    }) .catch(err => {
        res.status("Hashtag gagal dihapus.")
    })
}

//EXPORTS =======================================================================================

module.exports = {
    adminPage, showLoginAdmin, addAdmin, loginAdmin,
    showAllKategori, createKategori, formUpdateKategori, editKategori, deleteKategori,
    showAllHashtag, createHashtag, formUpdateHashtag, editHashtag, deleteHashtag,
    getDataBlocking,
    blockingUser,
    adminPage, showLoginAdmin, addAdmin, loginAdmin}
