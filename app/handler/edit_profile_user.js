const { user } = require('../model/user')
const jwt = require('jsonwebtoken')

const showProfileUser = (req, res) => {

    const tokenCookies = req.cookies.token
    let decryptedToken = null
    if (tokenCookies) {
        // cek token 
        decryptedToken = jwt.verify(tokenCookies, 'hahaha')
    }
    user.findOne({
        where: {username: req.params.username}
    })
    .then((result) => {
        res.status(200).render('edit_profile_user', {
            dataUser: result,
            user: decryptedToken
        })
    })
    .catch((err) => {
        res.status(400).render('edit_profile_user', {data: err})
    })
  }

const updateProfileUser = (req, res) => {
    let username = req.params.username
    const tokenCookies = req.cookies.token
    let decryptedToken = null
    if (tokenCookies) {
        // cek token 
        decryptedToken = jwt.verify(tokenCookies, 'hahaha')
    }
    user.update({
        nama_lengkap: req.body.nama_lengkap,
        tgl_lahir: req.body.tgl_lahir,
        nomor_telepon: req.body.nomor_telepon,
        gender: req.body.gender

    }, {where: 
            {username}
        })
    .then((result) => {
        res.status(200).redirect(`/user/edit-profile/${username}`)
    })
    .catch((err) => {
        res.status(400).json(err)
    })
}

module.exports = {showProfileUser, updateProfileUser}