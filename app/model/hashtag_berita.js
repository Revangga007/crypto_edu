const { Sequelize, DataTypes } = require('sequelize');
const config = require('../../config');

const { berita } = require('./berita')
const { hashtag } = require('./hashtag')

const db = new Sequelize(
  config.DB_NAME,
  config.DB_USERNAME,
  config.DB_PASSWORD,
  {
    host: config.DB_HOST,
    port: config.DB_PORT,
    dialect: config.DB_DIALECT,
    logging: false,
  }
);

let hashtag_berita = db.define('hashtag_berita', {
  id:{type: DataTypes.INTEGER, primaryKey: true,  autoIncrement: true},
  id_berita: {type: DataTypes.INTEGER, references:{
    model:berita,
    key:'id'
  }},
  id_hashtag: {type: DataTypes.INTEGER, references:{
    model:hashtag,
    key:'id'
  }}

  
}, {
  freezeTableName: true,
  timestamp: true
})


module.exports = {
  hashtag_berita
}