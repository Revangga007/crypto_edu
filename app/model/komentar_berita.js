const { Sequelize, DataTypes } = require('sequelize');
const config = require('../../config');

const db = new Sequelize(
  config.DB_NAME,
  config.DB_USERNAME,
  config.DB_PASSWORD,
  {
    host: config.DB_HOST,
    port: config.DB_PORT,
    dialect: config.DB_DIALECT,
    logging: false
  }
);

let komentar_berita = db.define('komentar_berita', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  id_berita: {type: DataTypes.INTEGER,allowNull: false},
  id_komentar: {type: DataTypes.INTEGER,allowNull: false}
}, {
  freezeTableName: true,
  timestamp: true
})

module.exports = {
  komentar_berita
}