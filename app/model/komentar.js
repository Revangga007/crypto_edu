const { Sequelize, DataTypes } = require('sequelize');
const config = require('../../config');

const db = new Sequelize(
  config.DB_NAME,
  config.DB_USERNAME,
  config.DB_PASSWORD,
  {
    host: config.DB_HOST,
    port: config.DB_PORT,
    dialect: config.DB_DIALECT,
    logging: false,
    operatorsAliases: false
  }
);

let komentar = db.define('komentar', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  id_user: {type: DataTypes.INTEGER, allowNull: false},
  isiKomentar: {type: DataTypes.TEXT, allowNull: false},
  isReported: {type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true}
}, {
  freezeTableName: true,
  timestamp: true
})

module.exports = {
  komentar
}