const { Sequelize, DataTypes } = require('sequelize');
const config = require('../../config');

const db = new Sequelize(
  config.DB_NAME,
  config.DB_USERNAME,
  config.DB_PASSWORD,
  {
    host: config.DB_HOST,
    port: config.DB_PORT,
    dialect: config.DB_DIALECT,
    logging: false,
    operatorsAliases: false
  }
);

let berita = db.define('berita', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  judul: {type: DataTypes.STRING(255), allowNull: false},
  subjudul: {type: DataTypes.STRING(255), allowNull: false},
  isiberita: {type: DataTypes.TEXT, allowNull: false},
  id_kategori_berita: {type: DataTypes.INTEGER, allowNull: false},
  slug: {type: DataTypes.STRING(255), allowNull: false},
  thumbnail: {type: DataTypes.STRING(255), allowNull: false},
  thumb_judul: {type: DataTypes.STRING(100), allowNull: false, defaultValue: true},
  isDraft: {type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true}
}, {
  freezeTableName: true,
  timestamp: true
})




module.exports = {
  berita
}