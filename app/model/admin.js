const { resolveInclude } = require('ejs');
const { Sequelize, DataTypes } = require('sequelize');
const config = require('../../config');

const db = new Sequelize(
    config.DB_NAME,
    config.DB_USERNAME,
    config.DB_PASSWORD,
    {
        host: config.DB_HOST,
        port: config.DB_PORT,
        dialect: config.DB_DIALECT,
        logging: false
    }
);

let admin = db.define('admin', {
    id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true},
    email: {
        type: DataTypes.STRING(50),
        allowNull: false,
        unique: true},
    password: {
        type: DataTypes.STRING(255),
        allowNull: false},
    role: {
        type: DataTypes.INTEGER,
        allowNull: false},
    createdAt: {
        type: DataTypes.DATE},
    updatedAt: {
        type: DataTypes.DATE},
    isBlocked: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false}
}, {
    freezeTableName: true
})

module.exports = {
    admin
}