const { Sequelize, DataTypes } = require('sequelize');
const config = require('../../config');

const db = new Sequelize(
  config.DB_NAME,
  config.DB_USERNAME,
  config.DB_PASSWORD,
  {
    host: config.DB_HOST,
    port: config.DB_PORT,
    dialect: config.DB_DIALECT,
    logging: false,
  }
);

let kategori = db.define('kategori', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  nama_kategori: {type: DataTypes.STRING(100), allowNull: false}
}, {
  freezeTableName: true,
  timestamp: true
})

module.exports = {
  kategori
}
