const { Sequelize, DataTypes } = require('sequelize');
const config = require('../../config');

const db = new Sequelize(
  config.DB_NAME,
  config.DB_USERNAME,
  config.DB_PASSWORD,
  {
    host: config.DB_HOST,
    port: config.DB_PORT,
    dialect: config.DB_DIALECT,
    logging: false,
    operatorsAliases: false
  }
);

let user = db.define('user', {
  id: {type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true},
  email: {type: DataTypes.STRING(50), allowNull: false, unique: true},
  password: {type: DataTypes.STRING(255), allowNull: false},
  nama_lengkap: {type: DataTypes.STRING(50), allowNull: false},
  nomor_telepon: {type: DataTypes.STRING(16), allowNull: false},
  username: {type: DataTypes.STRING(25), allowNull: false, unique: true},
  tgl_lahir: {type: DataTypes.DATE, allowNull: false},
  gender: {type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true},
  isBlocked: {type: DataTypes.BOOLEAN, allowNull: false, defaultValue: true}
}, {
  freezeTableName: true,
  timestamp: true
})

module.exports = {
  user
}
