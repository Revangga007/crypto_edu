insert into berita ("judul", "subjudul", "isiberita", "id_kategori_berita", "slug", "thumbnail", "thumb_judul", "isDraft", "createdAt", "updatedAt")
values (
	'El Salvador Serok Bitcoin, Harga Diprediksi Naik',
  	'El Salvador telah membeli 21 Bitcoin untuk menandai acara khusus abad ke-21', 
  	'El Salvador telah membeli 21 Bitcoin untuk menandai acara khusus abad ke-21. Pernyataan tersebut diumumkan langsung oleh Nayib Bukele selaku presiden negara setempat pada hari ini. Aksi serok Bitcoin tersebut tidak dilakukan tanpa sebab. Itu merupakan sebuah simbolis yang negara lakukan untuk menandai hari ke-21 di tahun ke-21 di abad ke-21.
Namun untuk aksi beli semacam ini, El Salvador telah melakukannya beberapa kali di masa lalu. 21 Bitcoin yang kurang lebih bernilai $1 juta itu, hanyalah ukuran kecil untuk pemerintah setempat. Pada Oktober lalu, El Salvador bahkan pernah membeli 420 BTC ketika market sedang koreksi. Harga Bitcoin pun berdampak naik secara beransur setelah aksinya.
Lantas, akankah 21 Bitcoin yang mereka beli juga berdampak pada kenaikan harga pasar?
Dampak dari El Salvador Serok Bitcoin
Pembelian 21 Bitcoin kemarin, sebenarnya tidak terlalu menambah jumlah yang signifikan ke pundi-pundi negara. 
Namun, itu berhasil menunjukkan seberapa besar dedikasi pemerintahan setempat, terutama Presiden Bukele yang telah menerima pujian sekaligus kritik setelah keputusan kontroversinya mengizinkan Bitcoin sebagai alat pembayaran yang sah.
Sementara untuk aksi serok Bitcoin yang kerap kali dilakukan El Salvador, ternyata telah meningkatkan volume adopsi dari aset raksasa crypto tersebut dan berhasil mendorong harganya.
Para kritikus kemudian mengaitkan langkah itu dengan ketidakmampuan pemerintah untuk mendapatkan pinjaman dana sebesar $1,3 miliar dari International Monetary Fund (IMF). IMF sendiri secara terbuka telah terkenal skeptis terhadap taruhan Bitcoin yang berani di negara tropis itu.
Analis berpendapat bahwa obligasi Bitcoin dapat mempersulit negara-negara lain untuk mengamankan uang dari intuisi tradisional karena kelayakan kredit yang rendah. Namun pernyataan tersebut masih sebuah rencana.
El Salvador kini mulai memperluas jangkauannya dengan mengambil beberapa tindakan untuk meningkatkan infrastruktur disana.
Seperti yang diketahui sebelumnya, El Salvador telah memiliki lebih dari 200 ATM Bitcoin yang beroperasi secara aktif dan dalam waktu dekat berniat untuk menambah jumah tersebut.
Selain itu, ada juga dompet Chivo yang telah melampaui dua juta pengguna terhitung hingga bulan ini. Terlepas dari protes besar terhadap implementasi Bitcoin dan masalah teknologi yang dihadapi oleh dompet Chivo, para pendukung crypto berpendapat bahwa eksperimen besar di negara tersebut bisa menjadi gambaran dunia di masa depan.

',
  1,
  'el-salvador-serok-bitcoin-harga-diprediksi-naik',
  'bitcoin-1.jpeg',
  'El Salvador Serok Bitcoin',
  false,
  now(),
  now()
),
(
	'Melihat Nasib Crypto di 2022, Mati karena Bear Market?',
  	'pasar crypto yang membuat potensi depresiasi harga secara signifikan di Tahun 2022', 
  	'Menjelang akhir Tahun 2021 banyak investor crypto terutama pemula, yang mulai khawatir terhadap crypto yang dimilikinya.Hal ini disebabkan banyak analis yang menyatakan adanya siklus empat tahun di pasar crypto yang membuat potensi depresiasi harga secara signifikan di Tahun 2022.Walau prediksi tersebut bisa jadi benar, perlu diketahui bahwa ada banyak kemungkinan yang akan terjadi di Tahun 2022.Dalam artikel ini akan diberikan tiga kemungkinan yang akan terjadi di Tahun 2022 untuk memprediksi bagaimana keberlangsungan crypto nantinya.
    Salah satu faktor yang akan membuat koreksi cukup signifikan kemungkinan besar adalah kebijakan dari Bank Sentral Amerika. Seperti yang bisa dilihat, Dolar Amerika memiliki pengaruh yang besar terhadap perekonomian dunia akibat digunakan sebagai alat tukar utama dalam perdagangan internasional. Selain itu, mayoritas investor di seluruh dunia menggunakan Dolar Amerika untuk mengamankan kekayaannya, yang membuat muncul istilah mata uang pengaman atau safe-haven currency. Sehingga apa pun kebijakan dari Pemerintah dan Bank Sentral Amerika, akan berpengaruh cukup signifikan terhadap dunia, termasuk aset keuangan seperti crypto dan saham.

    Hal ini disebabkan kategori aset seperti saham dan crypto serta aset investasi lain yang masuk ke kategori aset berisiko. Kategorisasi tersebut membuatnya berkorelasi negatif dengan Dolar Amerika. Dalam era krisis pandemi ini, mayoritas pemerintah dan bank sentral menerapkan kebijakan ekspansif yaitu menambah jumlah uang beredar untuk mendorong perekonomian.Oleh karena itu di Tahun 2021 investor melihat apresiasi tinggi dalam beberapa aset berisiko seperti crypto dan saham.

    Dikabarkan bahwa Tahun 2022 ada kemungkinan perubahan kebijakan menjadi kontraktif yaitu ingin mengurangi jumlah uang beredar, terutama oleh Amerika.Sebab untuk saat ini angka inflasi sudah naik drastis terutama untuk Dolar Amerika. Jika kebijakan berubah, ada kemungkinan jumlah Dolar Amerika berkurang dan nilainya naik. Kondisi ini dapat menyebabkan depresiasi tinggi di nilai crypto karena korelasinya yang negatif dengan Dolar Amerika. Jadi ada kemungkinan di Tahun 2022, bersama dengan berakhirnya siklus empat tahun, mayoritas crypto akan mengalami depresiasi.
    ',
    1,
    'melihat-nasib-crypto-di-2022-mati-karena-bear-market?',
    'bitcoin-2.jpeg',
    'Melihat Nasib Crypto di 2022',
    false,
    now(),
    now()
),
(
	'Prediksi Harga Bitcoin 2022, Bisa $100.000?',
  	'Bitcoin tetap menjadi cryptocurrency paling dominan di dunia berdasarkan kapitalisasi pasar', 
  	'Bitcoin tetap menjadi cryptocurrency paling dominan di dunia berdasarkan kapitalisasi pasar.Jika ditarik ke belakang, awal tahun 2021 Bitcoin mulai  diperdagangkan sekitar $32.0000 per koin setelah melampaui tertinggi sepanjang masa 2017 sebesar $20.000 menjelang akhir tahun 2020.

    Cryptocurrency terkemuka melanjutkan reli 2020 pada tahun 2021 dan dengan cepat mencapai harga $64.000 pada bulan April, dilanjutkan dengan harga tertinggi sepanjang masa yang baru di atas $68.000 pada November. Melihat berbagaio faktor yang membuat harga Bitcoin meraih harga tertinggi berkali-kali, tentu sebagai investor menantikan gebrakan harga Bitcoin tahun depan, apakah bisa mengalahkan tahun ini atau justru merosot tajam dan memulai bear market?

    Dikutip dari Yahoo Finance, indikator jangka panjang menunjukkan  secara teknis harga Bitcoin dapat meningkat selama beberapa minggu dan bulan mendatang karena Bitcoin diperkirakan akan berkinerja lebih baik di tahun mendatang.Pada bulan Januari, tekanan jual dari China diperkirakan akan berakhir, dan mirip dengan tingkat hash penambangan, harga Bitcoin dapat untuk melonjak lebih tinggi.

    Setelah itu terjadi, MACD dan RSI akan kembali ke wilayah positif. Menurut perkiraan WalletInvestor 2022, harga Bitcoin dapat mengakhiri perdagangan 2022 di atas level $80.000.

    Faktor ekonomi normal mempengaruhi harga cryptocurrency sama seperti mata uang atau investasi lainnya  penawaran dan permintaan, sentimen publik, siklus berita, peristiwa pasar, kelangkaan, dan banyak lagi.
    ',
    1,
    'prediksi-harga-bitcoin-2022-Bisa-100000',
    'bitcoin-3.jpg',
    'Prediksi Harga Bitcoin 2022',
    false,
    now(),
    now()
),
(
	'Inflasi Tak Bisa Dihindari, Miliarder Beralih ke Bitcoin',
  	'Miliarder kini semakin beralih ke Bitcoin dan saudara-saudaranya sebagai lindung nilai terhadap kekhawatiran inflasi mata uang fiat. ', 
  	'Miliarder kini semakin beralih ke Bitcoin dan saudara-saudaranya sebagai lindung nilai terhadap kekhawatiran inflasi mata uang fiat. Salah satu contohnya adalah miliarder kelahiran Hungaria Thomas Peterffy yang, dalam laporan Bloomberg 1 Januari, mengatakan bahwa akan lebih bijaksana untuk memiliki 2-3% dari portofolio seseorang dalam aset crypto untuk berjaga-jaga jika fiat tidak bisa diandalkan.

    Perusahaan Peterffy, Interactive Brokers Group Inc., mengumumkan bahwa mereka akan menawarkan perdagangan crypto kepada kliennya pada pertengahan 2020 menyusul peningkatan permintaan untuk kelas aset.Perusahaan saat ini menawarkan Bitcoin, Ethereum, Litecoin, dan Bitcoin Cash, tetapi akan memperluas pilihan itu dengan 5-10 koin lagi bulan ini.

    Peterffy, yang memegang jumlah crypto yang tidak diungkapkan sendiri, mengatakan bahwa ada kemungkinan aset digital dapat menuai “pengembalian luar biasa” bahkan jika beberapa juga bisa menjadi nol menurut Bloomberg. Pendiri Bridgewater Associates Ray Dalio adalah miliarder terkenal lainnya yang mengungkapkan portofolionya berisi beberapa Bitcoin dan Ethereum tahun lalu.Kemudian ada Microstrategy, korporat terbesar, yang memegang 124.391 BTC senilai sekitar $5,8 miliar menurut BitcoinTreasuries. Tesla yang berada di posisi kedua memegang sekitar 43.200 koin senilai sekitar $2 miliar dengan harga saat ini.
    ',
    1,
    'inflasi-tak-bisa-dihindari-miliarder-beralih-ke-bitcoin',
    'bitcoin-4.jpeg',
    'Inflasi Tak Bisa Dihindari',
    false,
    now(),
    now()
),
(
	'Kilas Balik Industri Crypto Indonesia 2021',
  	'Tahun 2021, diperkirakan tingkat kepemilikan kripto global rata-rata 3,9%, dengan lebih dari 300 juta pengguna kripto di seluruh dunia.', 
  	'Tahun 2021, diperkirakan tingkat kepemilikan kripto global rata-rata 3,9%, dengan lebih dari 300 juta pengguna kripto di seluruh dunia. Lebih dari 18.000 bisnis sudah menerima pembayaran cryptocurrency. India memimpin dengan 100 juta pengguna, disusul oleh USA dengan 27 juta pengguna, dan Nigeria 13 juta.

    Lalu, bagaimana penyerapan crypto di Indonesia?

    Dilansir dari Triple A, Indonesia ada di peringkat 30 besar, di bawah Malaysia dan Vietnam.  Diperkirakan ada 7,2 juta orang Indonesia yang memiliki cryptocurrency.

    Sedangkan menurut data dari Asosiasi Blockchain Indonesia, per Juli 2021 jumlah pemilik kripto di Indonesia berjumlah 7,4 juta orang meningkat  85% dari 2020 yang berjumlah 4 juta orang.

    Jumlah tersebut lebih banyak dari investor saham yang memiliki 2,7 juta investor berdasarkan data dari Bursa Efek Indonesia.  Sementara itu data dari Indodax menunjukan bahwa jumlah investor crypto di 2021 meningkat 99,76%, sebelumnya di akhir 2020 berjumlah 2.2 juta, dan di November tahun ini ada di angka 4.7 juta.

    Mengingat pertumbuhan pengguna aset kripto di Indonesia, pemerintah melalui Bappebti dan Dirjen Pajak pun tengah mempertimbangkan soal pengenaan pajak kepada para pemilik kripto di dalam negeri.

    Untuk saat ini penetapan pajak crypto dan sedang melalui tahap diskusi dengan beberapa pelaku pasar seperti bursa hingga asosiasi.

    Dalam publikasi Bappebti disebutkan jika pajak crypto di Indonesia direncanakan akan berada pada tarif 0,05%, pajak ini lebih rendah daripada saham yang dikenakan 0,1%. 

    Sedangkan kabar terbaru yang dihimpun dari berbagai sumber, pemerintah mulai melakukan pembahasan pengenaan Pajak Penghasilan (PPh) final bagi masyarakat yang berinvestasi di aset kripto.

    Di mana besaran PPh mulai mengerucut ke angka 0,03%. Hingga saat ini belum ada aturan pasti mengenai pajak crypto di Indonesia, 

    ',
    1,
    'inflasi-tak-bisa-dihindari-miliarder-beralih-ke-bitcoin',
    'bitcoin-5.jpeg',
    'Inflasi Tak Bisa Dihindari',
    false,
    now(),
    now()
),