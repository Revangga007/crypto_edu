insert into berita ("judul", "subjudul", "isiberita", "id_kategori_berita", "slug", "thumbnail", "thumb_judul", "isDraft", "createdAt", "updatedAt")
values (
	'Solana Gantikan Posisi XRP Pada Peringkat Cryptocurrency!',
  	'Hanya dalam satu hari saja, pasar cryptocurrency secara keseluruhan kehilangan lebih dari 11% dari total kapitalisasinya', 
  	'Menjelang akhir pekan, para trader berharap episode bearish beberapa hari terakhir akan segera dilupakan. 
    Hanya dalam satu hari saja, pasar cryptocurrency secara keseluruhan kehilangan lebih dari 11% dari total kapitalisasinya. Koin-koin utama pun sedang berada dalam ekosistem yang menyedihkan.
    Tapi, tidak dengan Solana.
    Terlepas dari perjuangan yang dihadapi sebagian besar proyek crypto saat ini, Solana membuktikan tetap eksis dan terus tumbuh dan berkembang.
    Menurut data dari Coinecko, blockchain yang menjanjikan telah mencatat kinerja harga terbaik dari 10 cryptocurrency teratas selama 7 hari terakhir, yang mana ada sekitar 86,9% indikasi nilai yang meningkat. 
    Tak cukup sampai disana, token Solana juga menyalip XRP dalam peringkat cryptocurrency dan mengklaim tempat ke-6 dalam daftar untuk pertama kalinya.
    Solana dan Kejayaanya
    Melihat grafik diatas, harga Solana terpantau melebihi semua ekspektasi. Kenaikannya hampir parabola dan dengan hasil mingguan yang semakin besar sejak awal Agustus.
    SOL memulai minggu ini di $141 dan saat ini harganya sudah melebihi $208.
    Indikator tersebut menunjukkan bahwasanya token SOL telah mencapai “titik jenuh” beli tertinggi sepanjang sejarahnya.
    Untuk itu, trader disarankan untuk berhati-hati jika mengejar posisi beli. Sementara indikator kekuatannya seperti ADX, menunjukkan bahwa token akan sangat, sangat bullish.
    Menelaah dari sudut pandang XRP, XRP memang tidak berkinerja baik minggu ini. Selain bergerak di bawah aarahan Bitcoin, masalah hukum juga membuatnya tidak banyak menguntungkan.
    Grafik yang diperbaharui pada 7 September 2021 itu, memperlihatkan XRP yang mengalami penurunan harga terburuk sejak Mei. 
    XRP juga kehilangan lebih dari 18% dari harganya kemudian naik dari $ 1,38 menjadi $ 1,12.  Hari-hari berikutnya juga sama, bearish. 
    Saat ini XRP dilaporkan diperdagangkan hanya satu dolar per koin. Kecelakaan ini berkontribusi pada perubahan posisi dalam peringkat cryptocurrency.

    ',
  4,
  'solana-gantikan-posisi-XRP-pada-peringkat-cryptocurrency',
  'solana-1.jpeg',
  'Solana Gantikan Posisi XRP',
  false,
  now(),
  now()
),
(
	'Soldex, Decentralized Exchange Terbaru di Jaringan Solana',
  	'Decentralized Exchange (DEX) merancang Soldex untuk memfasilitasi pengguna dengan membawa salah satu aset cryptocurrency andalan, Solana', 
  	'Decentralized Exchange (DEX) merancang Soldex untuk memfasilitasi pengguna dengan membawa salah satu aset cryptocurrency andalan, Solana. 
    DEX juga menggunakan kecerdasan buatan (Al) untuk membuat perdagangan lebih mudah diakses. 
    Saat ini, pengguna baru harus belajar tentang pasar dan aset yang ingin mereka perdagangkan lebih dahulu. Sepertinya, DEX melihat ini sebagai sebuah hambatan untuk pemula.
    Oleh sebab itu, DEX membuat inovasi Soldex sebagai jawaban dari permasalahan hambatan tersebut.
    Inovasi ini akan memberi trader kemampuan untuk menggunakan algoritma perdagangan bertenaga AI yang dikembangkan oleh penggunanya sendiri.
    Di luar kemampuan perdagangan AI yang inovatif, Soldex menawarkan lebih banyak fitur tambahan dibandingkan dengan inovasi DEX sebelumnya. 
    Karena dibangun secara native di blockchain Solana, ini menyelesaikan banyak masalah infrastruktur yang terlihat pada sistem yang lebih lama.
    Dan agar tetap terdesentralisasi, infrastruktur internal yang terdiri dari smart contract dan penggunaan oracle.
    Sepanjang dekade terakhir, pertukaran terus berkembang mengikuti perkembangan pasar kripto. Solana dan DEX kemudian membagikan tanggapan tersebut di platform mereka, 
    “Anda dapat mengalami transaksi waktu nyata, menyetor dengan mudah menggunakan dompet pertukaran pribadi, dan melakukan pertukaran kecepatan ringan. Hal ini juga memungkinkan investor untuk berpartisipasi dalam tata kelola masyarakat dan likuiditas insentif. Yang terakhir berarti bahwa pengguna dapat mengunci dana mereka untuk jangka waktu tertentu dan mendapatkan bunga atas investasi tersebut.”
    ',
    4,
    'soldex-decentralized-exchange-terbaru-di-jaringan-solana',
    'solana-2.jpeg',
    'Soldex Decentralized Exchange Terbaru',
    false,
    now(),
    now()
),
(
	'Solana VS Polkadot, Mana yang Jadi Primadona Investor?',
  	'Biasa disandingkan dengan Cardano, Solana kini ganti rival bersama Polkadot dan untuk bersaing menjadi primadona investor masing-masing.', 
  	'Biasa disandingkan dengan Cardano, Solana kini ganti rival bersama Polkadot dan untuk bersaing menjadi primadona investor masing-masing. 
    Fenomena ini terjadi setelah koin DOT mencatat keuntungan tinggi di kuartal 3 tahun ini. Dengan kenaikan 150% sepanjang Agustus dan September, minat institusional juga memuncak.
    Solana VS Polkadot
    Menurut data yang dilansir dari Messari, aset crypto yang paling sering dimiliki oleh investor sepanjang kuartal 3 tahun ini adalah Polkadot.
    Sedangkan jika ditinjau dari aset teratas yang diinvestasikan, Solana memiliki circulating market cap paling tinggi yang memimpin semua altcoin. 
    Solana juga menunjukkan ROI v. USD yang tinggi sebesar 9837,50%, serta memiliki kinerja yang solid sepanjang tahun.
    Sementara Polkadot sangat terbantu oleh lelang slot parachain-nya yang meningkatkan volume transaksi pada jaringan.
    Koin DOT bahkan sempat sentuh harga tertingginya sepanjang masa karena pembaruan tersebut.
    ',
    4,
    'solana-vs-polkadot-mana-yang-jadi-primadona-investor',
    'solana-3.jpeg',
    'Solana VS Polkadot',
    false,
    now(),
    now()
),
(
	'Melesat 13.000 Persen Sejak Awal Tahun, Apa Itu Solana?',
  	'Harga mata uang kripto solana (SOL) dalam beberapa waktu terakhir menarik minat para investor aset kripto. ', 
  	'JAKARTA, KOMPAS.com - Harga mata uang kripto solana (SOL) dalam beberapa waktu terakhir menarik minat para investor aset kripto. Pasalnya, harga saham mata uang kripto tersebut terus mengalami kenaikan ketika tren pasar sedang koreksi. Harga solana sempat menyentuh rekor tertinggi pada 9 September 2021 lalu, yakni menjadi di kisaran 214,96 dollar AS per keping.

    Angka tersebut setara dengan sekitar Rp 3,05 juta. Bila dibandingkan dengan 1 Januari 2021 lalu, rekor harga solana tersebut telah menguat hingga lebih dari 13.200 persen. Pada awal tahun lalu, solana diperdagangkan di kisaran 1,61 dollar AS per keping. Adapun hari ini Minggu (12/9/2021), harga solana tengah mengalami koreksi, menjadi di kisaran 178,79 dollar AS per keping. Meski demikian, harga solana masih menguat lebih dari 11.000 persen bila dibandingkan dengan harga awal tahun.

    Dilansir dari Forbes, salah satu penyebab harga solana meroket adalah proyek blockchain yang menjadi pusat dari aset kripto tersebut terus berlanjut dan hasilnya kian terlihat. Proyek blockchain solana diklaim merupakan blockchain tercepat di dunia.

    Berdasarkan dokumen solana dijelaskan, jaringan tersebut secara teoritis mampu memproses 710.000 transaksi per detik dengan 1 gigabit per detik koneksi jaringan.

    ',
    4,
    'melesat-13000-persen-sejak-awal-tahun-apa-itu-solana',
    'solana-4.jpeg',
    'apa itu solana',
    false,
    now(),
    now()
),
(
	'Dukung Ekosistem NFT di Indonesia, Solana Gelar Kompetisi Seni dengan Total Hadiah Rp 1 Miliar',
  	'Beberapa tahun belakang, platform teknologi berbasis blockchain banyak bermunculan.', 
  	'KOMPAS.com – Beberapa tahun belakang, platform teknologi berbasis blockchain banyak bermunculan. Adapun teknologi blockchain merupakan jaringan terdesentralisasi yang memungkinkan pengguna untuk memanfaatkan sekaligus memiliki suatu konten digital secara bersamaan. 

    Dalam teknologi blockchain, terdapat istilah non-fungible token (NFT). NFT merujuk pada sebuah token unik yang merepresentasikan kepemilikan obyek digital, baik berupa karya seni gambar, musik, cuplikan video, avatar virtual, maupun video game.

    Saat ini, NFT berkembang pesat di banyak negara di dunia. Diberitakan Kontan.co.id, Senin (11/10/2021), volume penjualan NFT di dunia mencapai 10,7 miliar dollar Amerika Serikat (AS) atau setara Rp 153 triliun pada kuartal III 2021. 

    Indonesia pun menjadi salah satu negara yang menyambut positif pasar NFT. Sebab, banyak seniman dan kreator konten dalam negeri yang menjual karya seninya melalui platform blockchain. 

    Dari beberapa platform blockchain, Solana menjadi salah satu platform yang gencar mendukung kreator konten asal Indonesia. Terbaru, Solana bahkan mengadakan kompetisi seni NFT bertajuk Indonesia Art Project dengan total hadiah Rp 1 miliar. 
    Solana Labs’ Ecosystem Growth Tamar Menteshashvili mengatakan, melalui kompetisi seni NFT tersebut, kreator konten asal Indonesia bisa mengirimkan karyanya dan berpeluang mendapatkan hadiah uang.

    ',
    4,
    'dukung-ekosistem-nft-di-indonesia-solana-gelar-kompetisi-seni-dengan-total-hadiah-rp-1-miliar',
    'solana-5.jpeg',
    'Solana Gelar Kompetisi Seni',
    false,
    now(),
    now()
),