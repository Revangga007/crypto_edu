insert into berita ("judul", "subjudul", "isiberita", "id_kategori_berita", "slug", "thumbnail", "thumb_judul", "isDraft", "createdAt", "updatedAt")
values 
(
	'Aksi Beli Tren Bullish, Harga Uang Kripto Cardano Melonjak 11%',
  	'Mata uang kripto Cardano diperdagangkan pada $1,4521', 
  	'Mata uang kripto Cardano diperdagangkan pada $1,4521 pukul 14:02 (07:02 GMT) di Investing.com Index pada hari Selasa, dan harga melonjak 10,79% untuk hari ini. Itu merupakan keuntungan satu hari terbesar sejak sejak 2 Desember.
  Lonjakan kenaikan tersebut telah mendorong kapitalisasi pasar Cardano bertambah menjadi $48,3910B, atau 2,03% dari total kapitalisasi pasar cryptocurrency. Pada level tertinggi, market cap Cardano adalah $94,8001B.
  Cardano telah diperdagangkan pada kisaran harga $1,4199 hingga $1,4744 dalam 24 jam sebelumnya.
  Selama tujuh hari terakhir, Cardano telah menderita kejatuhan, dan menurun 7,25%. Volume Cardano yang diperdagangkan selama 24 jam terakhir hingga waktu penulisan adalah $2,7307B atau 1,96% dari 
  total volume seluruh mata uang kripto. Harga telah diperdagangkan di kisaran $1,1972 hingga $1,7565 dalam 7 hari terakhir.
  Pada harga saat ini, Cardano masih turun 53,14% dari level tertinggi sepanjang masa di $3,10 pada 2 September.
  ',
  5,
  'aksi-beli-tren-bullish-harga-uang-kripto-mardano-melonjak-1',
  'cardano-1.jpeg',
  'Aksi Beli Tren Bullish',
  false,
  now(),
  now()
),
(
	'Bitcoin Cs Ikutan Santa Rally, Cardano-Terra "to The Moon"',
  	'Harga Bitcoin, Ethereum, dan kripto berkapitalisasi pasar besar', 
  	'Harga Bitcoin, Ethereum, dan kripto berkapitalisasi pasar besar (big cap) lainnya terpantau menghijau pada perdagangan Jumat (24/12/2021) pagi hari waktu Indonesia, karena investor kembali memburu aset kripto jelang libur Natal tahun 2021.
  Melansir data dari CoinMarketCap per pukul 09:30 WIB, hanya koin digital (token) Avalanche yang terpantau melemah pada pagi hari ini. Avalanche melemah 1,5% ke level harga US$ 121,58/koin atau setara dengan Rp 1.722.789/koin (asumsi kurs hari ini Rp 14.170/US$).
  Sedangkan sisanya terpantau cerah bergairah pada pagi hari ini. Bitcoin melonjak 5,42% ke level harga US$ 51.275,07/koin atau setara dengan Rp 726.567.742/koin, Ethereum melesat 3,09% ke level US$ 4.113,97/koin atau Rp 58.294.955/koin, Binance Coin menguat 2,38% ke US$ 549,22/koin (Rp 7.782.447/koin).
  Berikutnya Solana melompat 3,59% ke US$ 188,42/koin (Rp 2.669.911/koin), Cardano terbang 10,02% ke US$ 1,48/koin (Rp 20.972/koin), XRP terapresiasi 0,82% ke US$ 0,9896/koin (Rp 14.023/koin), dan Terra meroket 13,71% ke US$ 98,88/koin (Rp 1.401.130/koin).
  Seperti di pasar saham global, pasar kripto pada pagi hari ini juga cenderung membentuk pola "Santa Claus Rally", di mana Bitcoin berhasil menyentuh kembali level psikologisnya di US$ 50.000, setelah hampir sepekan lebih Bitcoin berusaha menggapai kembali level tersebut.
  Tak hanya Bitcoin saja, Ethereum dan kripto big cap lainnya cenderung cerah pada hari ini. Di Ethereum, harganya kembali ke atas kisaran level US$ 4.000 pada pagi hari ini.
  Positifnya pasar kripto tak lepas dari sikap investor yang kembali memburu aset berisiko setelah pada pekan lalu sempat terkoreksi.
  ',
    5,
    'bitcoin-cs-ikutan-santa-rally-cardano-terra',
    'cardano-2.jpeg',
    'Bitcoin Cs Ikutan Santa Rally',
    false,
    now(),
    now()
),
(
	'Bukan Bitcoin dan Ethereum, Ini 2 Kripto yang Diprediksi Punya Prospek Cerah',
  	'Para investor kripto tentu bertanya-tanya, cryptocurrency mana yang bisa terbang tinggi', 
  	'Para investor kripto tentu bertanya-tanya, cryptocurrency mana yang bisa terbang tinggi dalam jangka panjang. The Motley Fool merekomendasikan dua kripto, yakni The Sandbox (SAND) dan Cardano (ADA).
  Kedua aset terlihat siap untuk meroket dalam jangka panjang karena mereka membangun posisi kepemimpinan dalam metaverse berbasis blockchain dan pengembangan aplikasi terdesentralisasi.
  1.      The Sandbox
  Naik sebesar 110 persen selama 30 hari terakhir, Sandbox sudah berada dalam performa yang bagus. Sementara itu valuasi cryptocurrency terkenal tidak stabil, token yang berfokus pada metaverse proyek ini dapat mempertahankan tingkat pertumbuhan yang luar biasa berkat keuntungan penggerak awal dan kasus penggunaan potensial sebagai landasan dunia virtual yang terdesentralisasi.
  Metaverse adalah iterasi internet generasi berikutnya yang potensial yang akan mendukung realitas virtual yang saling berhubungan. Dan hal itu bisa mengubah industri seperti game dan media sosial dengan membuatnya lebih imersif.
  Sandbox bertujuan untuk mengatasi peluang ini dengan menciptakan platform berbasis blockchain di mana pemain dapat "membangun, memiliki, dan memonetisasi" pengalaman bermain game -- mirip dengan perusahaan video game publik Roblox. 
  Sandbox juga memungkinkan pengguna untuk membeli real estat virtual yang dikenal sebagai "LAND" di metaverse-nya. Kepemilikan dicatat melalui token non-fungible (NFT), yang merupakan catatan digital yang disimpan di blockchain.
  Sandbox terus membuat kemajuan yang mengesankan. Pada 29 November, platform ini meluncurkan versi Alpha dari dunia virtualnya, yang menampilkan 18 pengalaman game dan akan berjalan hingga 19 Desember. Pengembangnya Pixowl (anak perusahaan pengembang game yang berbasis di Hong Kong, Animoca Brands) juga memiliki pengalaman dunia nyata dalam mengembangkan game seluler (waralaba Sandbox) untuk Android dan Apple IOS, yang menambah kredibilitasnya.
  2.  	Cardano
  Dengan kapitalisasi pasar senilai 51 miliar Dollar AS, Cardano adalah cryptocurrency terbesar keenam saat ini di dunia. Kripto ini bertujuan untuk bersaing dengan Ethereum dalam menarik program otonom yang dibuat pengguna yang disebut aplikasi terdesentralisasi (dApps) dan menawarkan kecepatan dan skalabilitas yang lebih baik daripada saingannya yang lebih besar.
  DApps secara dramatis meningkatkan potensi teknologi blockchain di luar hanya menyimpan dan mentransmisikan nilai. Mereka memungkinkan kasus penggunaan mulai dari pertukaran cryptocurrency terdesentralisasi hingga platform keuangan yang memungkinkan investor untuk meminjam dan meminjamkan kepemilikan cryptocurrency mereka.
  DApps Cardano menggunakan token asli jaringan (ADA) untuk berinteraksi dan membayar biaya di jaringannya, sehingga mereka akan berdampak langsung pada permintaan aset dan penilaiannya.
  Cardano mengaktifkan kontrak pintar (program yang dijalankan sendiri yang digunakan untuk membuat dApps) melalui pemutakhiran Alonzo pada bulan September. Dan mungkin perlu beberapa tahun bagi platform untuk membanggakan ekosistem dApp yang berkembang dengan baik seperti saingannya yang lebih besar, Ethereum, yang menampung lebih dari 3.000 proyek.
  Akan tetapi Cardano bertujuan untuk menutup celah dengan menawarkan fungsionalitas yang unggul. Tidak seperti Ethereum, yang menggunakan protokol proof-of-work (POW) yang rumit di mana para penambang memecahkan teka-teki untuk memvalidasi transaksi, Cardano menggunakan sistem proof-of-stake yang disebut Ouroboros. Di sini, otoritas dalam melakukan validasi transaksi berasal dari kepemilikan token Cardano.
  Penambang Cardano memperbarui blockchain menggunakan koin yang ada, yang lebih cepat (Cardano dapat memproses 257 transaksi per detik dibandingkan dengan Ethereum yang memproses 15 hingga 20 transaksi) dan kurang berbahaya bagi lingkungan daripada sistem POW.
  Ethereum berencana untuk akhirnya beralih ke PoS, tetapi tidak jelas kapan itu akan terjadi, menjadikan Cardano platform yang menarik bagi pengembang dApp yang memprioritaskan kecepatan dan skalabilitas.
  ',
    5,
    'bukan-bitcoin-dan-ethereum-ini-2-kripto-yang-diprediksi-punya-prospek-cerah',
    'cardano-3.jpeg',
    'Bukan Bitcoin dan Ethereum, Ini 2 Kripto',
    false,
    now(),
    now()
),
(
	'Dipengaruhi Tren Negatif, Harga Uang Kripto Cardano Jatuh 11%',
  	'Harga mata uang kripto Cardano diperdagangkan pada $1,5677', 
  	'Harga mata uang kripto Cardano diperdagangkan pada $1,5677 pukul 02:43 (19:43 GMT) di Investing.com Index pada hari Jumat, dan harga jatuh 10,67% untuk hari ini. Itu merupakan persentase kerugian satu hari terbesar sejak sejak 7 September.
  Pergerakan tren kejatuhan tersebut telah mengurangi kapitalisasi pasar Cardano turun menjadi $52,7060B, atau 2,11% dari total kapitalisasi pasar cryptocurrency. Pada level tertinggi, market cap Cardano adalah $94,8001B.
  Cardano telah diperdagangkan pada kisaran harga $1,5657 hingga $1,7281 dalam 24 jam sebelumnya.
  Selama tujuh hari terakhir, Cardano tetap tidak berubah nilai, dan hanya bergerak 0,75%. Volume Cardano yang diperdagangkan selama 24 jam terakhir hingga waktu penulisan adalah $2,5174B atau 2,08% dari total volume seluruh mata uang kripto. Harga telah diperdagangkan di kisaran $1,4230 hingga $1,7565 dalam 7 hari terakhir.
  Pada harga saat ini, Cardano masih turun 49,41% dari level tertinggi sepanjang masa di $3,10 pada 2 September.
  ',
    5,
    'dipengaruhi-tren-negatif-harga-uang-kripto-cardano-jatuh-11',
    'cardano-4.jpeg',
    'Dipengaruhi Tren Negatif',
    false,
    now(),
    now()
),
(
	'Perdagangan Tren Bearish, Harga Uang Kripto Cardano Jatuh 10%',
  	'Harga mata uang kripto Cardano diperdagangkan pada $1,4369 pukul 02:51 (19:51 GMT)', 
  	'Harga mata uang kripto Cardano diperdagangkan pada $1,4369 pukul 02:51 (19:51 GMT) di Investing.com Index pada hari Sabtu, dan harga jatuh 10,06% untuk hari ini. Itu merupakan persentase kerugian satu hari terbesar sejak sejak 27 Oktober.
  Pergerakan tren kejatuhan tersebut telah mengurangi kapitalisasi pasar Cardano turun menjadi $47,9444B, atau 2,07% dari total kapitalisasi pasar cryptocurrency. Pada level tertinggi, market cap Cardano adalah $94,8001B.
  Cardano telah diperdagangkan pada kisaran harga $1,1972 hingga $1,5570 dalam 24 jam sebelumnya.
  Selama tujuh hari terakhir, Cardano telah menderita kejatuhan nilai, dan menurun 8,51%. Volume Cardano yang diperdagangkan selama 24 jam terakhir hingga waktu penulisan adalah $4,2702B atau 2,00% dari total volume seluruh mata uang kripto. Harga telah diperdagangkan di kisaran $1,1972 hingga $1,7565 dalam 7 hari terakhir.
  Pada harga saat ini, Cardano masih turun 53,63% dari level tertinggi sepanjang masa di $3,10 pada 2 September.
  ',
    5,
    'perdagangan-tren-bearish-harga-uang-kripto-cardano-jatuh-10',
    'cardano-5.jpeg',
    'Perdagangan Tren Bearish',
    false,
    now(),
    now()
)