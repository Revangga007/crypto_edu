insert into berita ("judul", "subjudul", "isiberita", "id_kategori_berita", "slug", "thumbnail", "thumb_judul", "isDraft", "createdAt", "updatedAt")
values (
	'Bitcoin dan Ethereum di Zona Merah, Bagaimana Prediksi pada 5 Januari 2022?',
  	'Bitcoin dan Ethereum serta beberapa kripto berkapitalisasi pasar utama lainnya sedang berada di zona merah', 
  	'Bitcoin dan Ethereum serta beberapa kripto berkapitalisasi pasar utama lainnya sedang berada di zona merah pada Selasa, (4/1/2022). Hal tersebut disebabkan ada lonjakan imbal hasil Treasury Amerika Serikat (AS) pada Senin malam 3 Januari 2022.

    Kenaikan imbal hasil surat berharga AS itu karena para pedagang bertaruh pada kenaikan suku bunga the Federal Reserve (the Fed) atau bank sentral AS meskipun kasus COVID-19 melonjak.

    Selain itu, hal ini juga disebabkan oleh beberapa investor yang cenderung beralih ke pasar saham karena optimisme pasar akan pemulihan ekonomi pada 2022.
    Sebelumnya, pasar kripto telah sepenuhnya memperhitungkan kenaikan suku bunga AS pertama pada Mei 2022, dan kedua pada akhir tahun 2022. 
    Chief Technology Officer Litedex Protocol, Aji M Iqbal menuturkan, peningkatan suku bunga AS cukup berpengaruh dapat menjadi ancaman bagi cryptocurrency atau aset kripto. 

    "Pasar menilai skenario kenaikan suku bunga AS yang lebih agresif atau setidaknya risikonya pada tahun 2022, dan itu pasti tetap menjadi ancaman bagi aset Kripto yang saat ini sedang digandrungi oleh para Investor, meskipun Covid-19 varian Omicron terus menyebar," ujar Iqbal dikutip dari keterangan tertulis, Selasa pekan ini.

    Selain itu, lonjakan kasus yang disebabkan oleh varian Omicron sangat berdampak pada perjalanan secara global dan layanan publik, serta menunda pembukaan beberapa sekolah di AS setelah liburan. Namun, investor tetap optimistis penguncian bisa dihindari. 

    Mengutip Coinmarketcap Selasa, 4 Januari 2022, harga cardano berada di level USD 1,31 per keping, dengan nilai kapitalisasi pasar sebesar USD 43,77 miliar. Jika diamati, dalam sepekan terakhir, harga cardano melemah hingga 8,28 persen.

    Aset kripto yang melemah lainnya adalah bitcoin (BTC) sebesar 1,98 persen dalam 24 jam terakhir ke level USD 46.119 per keping. Kemudian, ethereum (ETH) terkoreksi 1,99 persen dalam 24 jam terakhir ke level US$3.725 per keping.

    Binance coin (BNB) juga turun 3,79 persen dalam 24 jam terakhir menjadi USD 506,03 per keping.  Solana (SOL) turut mengalami pelemahan sebesar 1,76 persen dalam 24 jam terakhir ke harga USD 167,75 per keping. Aset kripto itu juga terkoreksi 11,74 persen dalam sepekan terakhir.

    Adapun, Ripple (XRP) yang turun hingga 2,84 persen dalam 24 jam terakhir menjadi USD 0,82 per keping. Diikuti dengan Terra (LUNA) yang melemah 4,15 persen dalam 24 jam terakhir ke level USD 87,05 per keping.

    Hingga perdagangan sore ini, Ethereum di platform Litedex protocol pada jam 15.00 WIB, melemah di harga  USD 3.753.70 (-1,22 persen) dengan volume transaksi sebesar USD 12,33 miliar dan kapitalisasi pasar USD 447,41 Miliar.

    Sedangkan untuk perdagangan Rabu, 5 Januari 2022, Ethereum akan dibuka fluktuatif namun ditutup melemah di kisaran  USD 3.650.30 - USD 3.896.50.
',
  2,
  'bitcoin-dan-ethereum-di-zona-merah',
  'eth-1.jpeg',
  'Bitcoin dan Ethereum di Zona Merah',
  false,
  now(),
  now()
),
(
	'Harga Aset Kripto Hari Ini: Ethereum Memimpin Kenaikan',
  	'Beberapa peringkat teratas cryptocurrency dalam kurun waktu 24 jam terakhir, menunjukkan pergerakan harga yang yang cukup baik.', 
  	'Beberapa peringkat teratas cryptocurrency dalam kurun waktu 24 jam terakhir, menunjukkan pergerakan harga yang yang cukup baik. Mayoritas aset kripto menguat cukup signifikan atau berada dalam zona hijau. 

Berdasarkan pantauan data dari Coinmarketcap, Rabu (5/1/2022), penguatan tertinggi dalam jajaran cryptocurrency teratas diamankan oleh Ethereum (ETH) dengan penguatan 1,89 persen dalam 24 jam terakhir ke level USD 3.815,15. Selain itu, rekam jejaknya dalam 7 hari terakhir juga memberikan hasil baik dengan penguatan sebesar 0,10 persen. 

Berdasarkan kinerja tersebut, Ethereum berhasil mengalahkan Bitcoin (BTC) dalam kurun waktu 24 jam dan 7 hari terakhir.

Bitcoin per hari ini telah mengalami sedikit penguatan sebesar 0,09 persen ke level USD 46.409,95 atau sekitar Rp 666,76 juta (asumsi kurs Rp 14.367 per dolar AS), sedangkan dalam 7 hari terakhir masih berada di zona merah yang melemah sebesar 3,18 persen. 

Menyusul Ethereum yang berada di posisi pertama, pada posisi kedua ada cardano (ADA) yang menguat 1,31 persen ke posisi USD 1,33 per koin. Namun, dalam seminggu ADA masih melemah 6,50 persen.

Selanjutnya penguatan ketiga dipegang oleh Binance coin (BNB) yang menguat 0,59 persen ke level USD 511.88 per koin. Meskipun begitu BNB dalam waktu 7 hari terakhir masih melemah sebesar 4,89 persen. 

Tidak ketinggalan XRP yang juga ikut menguat di hari ini dengan peningkatan sebesar 0,23 persen ke level USD 0,83 harga per koinnya. Secara keseluruhan, mayoritas jajaran cryptocurrency mengalami penguatan. 

Sebelumnya, Bitcoin dan Ethereum serta beberapa kripto berkapitalisasi pasar utama lainnya sedang berada di zona merah pada Selasa, 4 Januari 2022. Hal tersebut disebabkan ada lonjakan imbal hasil Treasury Amerika Serikat (AS) pada Senin malam 3 Januari 2022.

Kenaikan imbal hasil surat berharga AS itu karena para pedagang bertaruh pada kenaikan suku bunga the Federal Reserve (the Fed) atau bank sentral AS meskipun kasus COVID-19 melonjak. 

Selain itu, hal ini juga disebabkan oleh beberapa investor yang cenderung beralih ke pasar saham karena optimisme pasar akan pemulihan ekonomi pada 2022.
Sebelumnya, pasar kripto telah sepenuhnya memperhitungkan kenaikan suku bunga AS pertama pada Mei 2022, dan kedua pada akhir tahun 2022. 

Chief Technology Officer Litedex Protocol, Aji M Iqbal menuturkan, peningkatan suku bunga AS cukup berpengaruh dapat menjadi ancaman bagi cryptocurrency atau aset kripto. 

"Pasar menilai skenario kenaikan suku bunga AS yang lebih agresif atau setidaknya risikonya pada tahun 2022, dan itu pasti tetap menjadi ancaman bagi aset Kripto yang saat ini sedang digandrungi oleh para Investor, meskipun Covid-19 varian Omicron terus menyebar," ujar Iqbal dikutip dari keterangan tertulis, Selasa pekan ini.

Selain itu, lonjakan kasus yang disebabkan oleh varian Omicron sangat berdampak pada perjalanan secara global dan layanan publik, serta menunda pembukaan beberapa sekolah di AS setelah liburan. Namun, investor tetap optimistis penguncian bisa dihindari. 

Mengutip Coinmarketcap Selasa, 4 Januari 2022, harga cardano berada di level USD 1,31 per keping, dengan nilai kapitalisasi pasar sebesar USD 43,77 miliar. Jika diamati, dalam sepekan terakhir, harga cardano melemah hingga 8,28 persen.

Aset kripto yang melemah lainnya adalah bitcoin (BTC) sebesar 1,98 persen dalam 24 jam terakhir ke level USD 46.119 per keping. Kemudian, ethereum (ETH) terkoreksi 1,99 persen dalam 24 jam terakhir ke level US$3.725 per keping.

Binance coin (BNB) juga turun 3,79 persen dalam 24 jam terakhir menjadi USD 506,03 per keping.  Solana (SOL) turut mengalami pelemahan sebesar 1,76 persen dalam 24 jam terakhir ke harga USD 167,75 per keping. Aset kripto itu juga terkoreksi 11,74 persen dalam sepekan terakhir.

Adapun, Ripple (XRP) yang turun hingga 2,84 persen dalam 24 jam terakhir menjadi USD 0,82 per keping. Diikuti dengan Terra (LUNA) yang melemah 4,15 persen dalam 24 jam terakhir ke level USD 87,05 per keping.

Hingga perdagangan sore ini, Ethereum di platform Litedex protocol pada jam 15.00 WIB, melemah di harga  USD 3.753.70 (-1,22 persen) dengan volume transaksi sebesar USD 12,33 miliar dan kapitalisasi pasar USD 447,41 Miliar.

Sedangkan untuk perdagangan Rabu, 5 Januari 2022, Ethereum akan dibuka fluktuatif namun ditutup melemah di kisaran  USD 3.650.30 - USD 3.896.50.
    ',
    2,
    'harga-aset-kripto-hari-ini-ethereum-memimpin-kenaikan',
    'eth-2.jpeg',
    'Ethereum Memimpin Kenaikan',
    false,
    now(),
    now()
),
(
	'Harga Ethereum dan Bitcoin Pecah Rekor, Ini Penyebabnya',
  	'Harga Ethereum (ETH) dan Bitcoin (BTC) dilaporkan terus mengalami peningkatan', 
  	'Harga Ethereum (ETH) dan Bitcoin (BTC) dilaporkan terus mengalami peningkatan, dengan keduanya memecahkan rekor dengan melewati harga tertingginya pada Selasa (9/11/2021).

    Berdasarkan data yang dikumpulkan Indodax, harga Ethereum bertengger di angka Rp 68.279.000, sementara harga Bitcoin nyaris mencapai Rp 1 miliar atau Rp 968.392.000, pada hari Selasa.

    CEO Indodax Oscar Darmawan pun menyebut, pecah rekornya harga dua aset kripto ini merupakan kabar gembira bagi para investor.

    "Ethereum serta Bitcoin merupakan dua aset kripto dengan kapitalisasi pasar tertinggi saat ini," kata Oscar dalam siaran persnya.

    Ia mengatakan, dengan penguatan harga ini dan pecahnya rekor harga ETH dan BTC, menunjukkan investor percaya dengan fundamental dari kedua aset kripto ini sehingga terus melakukan aksi beli di pasar.

    Oscar menyebutkan, dibandingkan secara year to year atau November 2020 ke November 2021, harga ETH di 2020 hanya menyentuh angka sekitar Rp 8,8 juta dan BTC hanya menyentuh sekitar Rp 240 juta.

    "Itu tandanya kenaikan harga Ethereum sudah mencapai 675 persen dan kenaikan harga Bitcoin sudah mencapai 303,5 persen dari tahun sebelumnya," kata Oscar.

    Menurut Oscar, harga ETH yang naik bahkan sampai pecah rekor pada dasarnya karena pasar kripto saat ini sedang bullish, seiring dengan pecah rekornya harga Bitcoin.
    Oscar menjelaskan, sentimen utama yang membuat harga Ethereum dan Bitcoin naik karena adanya permintaan pasar yang sangat kuat. Selain itu, ada faktor lain yang berpotensi menguatkan harga keduanya.

    "Ethereum memang mengalami upgrade terus menerus khususnya di tahun ini. Setelah adanya upgrade Hard Fork London pada beberapa waktu lalu, upgrade pun kembali hadir untuk Ethereum yaitu Ethereum 2.0."

    Oscar mengungkapkan, dengan pembaruan Ethereum 2.0, Ethereum berubah menjadi proof of stake.

    Selain itu ada juga fitur autoburn yaitu berupa pemusnahan jumlah Ethereum yang ada untuk membatasi pasokan Ethereum dan memperluas jaringan Ethereum.
    Dengan adanya pembatasan ini, pasokan Ethereum yang akan beredar pun akan menjadi lebih langka.

    "Dengan langkanya pasokan Ethereum sementara permintaan di pasar semakin bertambah, tentu ini akan menaikkan harga dari Ethereum itu sendiri," kata Oscar.

    Di samping itu, menurut Oscar, Ethereum memiliki jaringan yang sudah banyak digunakan untuk pembuatan proyek blockchain ataupun koin kripto dan token baru.

    Sementara untuk Bitcoin, sentimen yang mendorong harga untuk terus menguat diindikasikan karena update blockchain Bitcoin bernama Taproot, yang akan aktif antara pekan ini atau pekan depan.

    Pembaruan Taproot ini akan meningkatkan tiga protokol peningkatan Bitcoin, yang menyoroti privasi pengguna, fitur smart contract, dan efisiensi biaya untuk memperoleh transaksi yang kompleks agar jauh lebih murah.

    "Dengan adanya peningkatan ini tentu menjadi salah satu faktor kuat pendorong para whale alias "investor besar" untuk berinvestasi di Bitcoin," kata Oscar.
    Sehingga ini merembet ke penguatan harga Bitcoin hari ini sampai akhirnya harganya pun tembus rekor tertinggi kembali.
    ',
    2,
    'harga-ethereum-dan-bitcoin-pecah-rekor',
    'eth-3.jpeg',
    'Harga Ethereum dan Bitcoin Pecah Rekor',
    false,
    now(),
    now()
),
(
	'Kripto Etherium Masih Berpotensi Melemah',
  	'Harga Etherium diprediksi masih terkoreksi mendekat USD 48.076,30 atau setara Rp 694,18 juta (asumsi kurs Rp 14.439 per dolar AS) pada Selasa, 7 Desember 2021', 
  	'Harga Etherium diprediksi masih terkoreksi mendekat USD 48.076,30 atau setara Rp 694,18 juta (asumsi kurs Rp 14.439 per dolar AS) pada Selasa, 7 Desember 2021.Dalam perdagangan Senin, 6 Desember 2021, secara teknikal Ethereum di platform Litedex protocol diperdagangkan masih melemah.
    
    Gerak ethereum pada kisaran harga USD 48.076,30-USD 49.309.35 atau setara Rp 712,02 juta.

    Harga bitcoin, ethereum dan kripto berkapitalisasi pasar besar (big cap) lainnya terpantau masih diperdagangkan cenderung melemah pada Senin pagi waktu Indonesia, meskipun beberapa kripto mulai kembali menguat tipis-tipis.

    Berdasarkan coinmarketcap.com, bitcoin (BTC) juga melemah 0,06 persen ke USD 48.953 per koin atau Rp 706,88 juta. Kemudian, binance coin (BNB) minus 2,2 persen ke USD 548,12 per koin dan solana (SOL) jatuh 0,88 persen ke USD193,29 per koin.

    Begitu pula dengan cardano (ADA) melemah 0,72 persen ke USD 1,37 per koin, ripple (XRP) terkontraksi 3,13 persen ke USD 0,8 per koin, dan dogecoin (DOGE) minus 0,9 persen ke USD 0,17 per koin. Tak ketinggalan, Shiba Inu (SHIB) dan binance USD (BUSD) juga berada di zona merah, masing-masing melemah 0,92 persen ke USD 0,00003 per koin dan minus 0,08 persen ke USD 0,99 per koin.

    Kendati begitu, beberapa kripto lainnya berhasill unjuk gigi di zona hijau. Ethereum (ETH) misalnya menguat 1,39 persen ke USD 4.157 per koin. Lalu, tether (USDT) naik tipis 0,04 persen ke USD 1 per koin. Kemudian, USD coin (USDC) menguat 0,02 persen ke USD 0,99 per koin dan polygon (MATIC) melejit 2 persen ke USD1,96 per koin.

    Forbes melaporkan, akibat kemerosotan tersebut, kapitalisasi pasar kripto menguap sekitar USD 300 miliar atau lebih dari Rp 4.300 triliun (kurs Rp 14.400 per dolar AS) hanya dalam tempo dua hari saja.

    Banyak yang prediksi kebijakan The Fed tersebut akan membuat bitcoin dan aset kripto lainnya rontok hingga tahun depan.

    Chief Technology Officer PT TFRX Garuda Berjangka, Aji M Iqbal mengatakan The Fed sedang melakukan tapering. Hal tersebut akan memicu koreksi di aset berisiko dan Bitcoin termasuk di dalamnya.

    "Semakin cepat The Fed melakukan tapering, maka kita akan melihat volatilitas yang tinggi di pasar saham dan obligasi, dan tentu saja bitcoin,"  kata Iqbal dalam keterangan tertulis, Senin (6/12/2021).

    Hal senada juga dikatakan Mike Novogratz, triliuner investor kripto dan CEO Galaxy Digital. Ia mengatakan kebijakan The Fed bisa membuat pasar kripto runtuh pada  2022.
    "Orang-orang kini menjadi bearish terhadap bitcoin dan mata uang kripto lainnya setelah penguatan tajam. Dalam satu tahun terakhir, bitcoin melesat nyaris 200 persen, ethereum 600 persen belum lagi yang lainnya juga naik ratusan persen," tutur Novogratz.
    ',
    2,
    'kripto-etherium-masih-berpotensi-melemah',
    'eth-4.jpeg',
    'Kripto Etherium Masih Berpotensi Melemah',
    false,
    now(),
    now()
),
(
	'Prediksi CEO Indodax Soal Aset Kripto Ethereum pada 2022',
  	'Selain Bitcoin, aset kripto dengan kapitalisasi pasar atau market cap terbesar adalah Ethereum (ETH).', 
  	'TSelain Bitcoin, aset kripto dengan kapitalisasi pasar atau market cap terbesar adalah Ethereum (ETH).

    CEO Indodax Oscar Darmawan dalam siaran persnya, dikutip Senin (3/1/2022) mengatakan, Ethereum saat ini sudah berevolusi menjadi Ethereum 2.0.

    Menurut Oscar, dengan evolusi ini, kecepatan, efisiensi, dan skalabilitas jaringan Ethereum bakal semakin meningkat sehingga bisa memproses lebih banyak transaksi dan mengurangi kemacetan.

    "Secara teknologi, harga, dan ekosistem sebenarnya Ethereum sudah mengungguli Bitcoin. Ethereum itu bagus ekosistemnya juga luar biasa dan dipakai di dunia institusi juga," kata Oscar.

    Meski begitu, Oscar mengatakan, yang jadi masalah apakah Ethereum akan bisa scale up lagi atau tidak, untuk menurunkan biaya transaksinya, karena biaya gas dari Ethereum ini adalah kuncinya.

    "Jika pada 2022 pengembang dari Ethereum ini bisa menurunkan gas fee-nya, saya kira ada kemungkinkan bahwa Ethereum bisa meng-off lap Bitcoin," kata Oscar.

    Data Indodax di penghujung Desember lalu, pada tanggal 28, Ethereum menyentuh kisaran angka Rp 58 juta per 1 ETH. Di awal Januari 2021, harganya 1 ETH hanya berkisar Rp 10 juta.

    Sehingga, dapat disimpulkan terdapat kenaikan sekitar 480 persen. Performa kripto ini pun dinilai sangat baik di 2021, bahkan sempat menyentuh all time high di angka 68 juta pada November 2021.
    Oscar sendiri berharap agar pada 2022, performa aset kripto bakal lebih baik dengan adanya ekosistem terbaru.

    Oscar memprediksi, di 2022 akan ada suatu ekosistem baru setelah pada 2020 ada DeFi, sementara pada 2021 ada hype NFT dan metaverse. Menurutnya ekosistem ini belum akan ditinggalkan meski ada yang baru.

    Tidak hanya soal ekosistem, setelah adanya pergerakan dari negara El Salvador yang menjadikan Bitcoin sebagai alat pembayaran yang sah, tentu akan ada negara lainnya yang menyusul.

    Pada 2021, Bitcoin juga dianggap sudah semakin mainstream. Banyak orang awam yang awalnya tidak tahu, mulai mendengar dan aware soal mata uang kripto ini.
    Bitcoin pun juga sudah digunakan sebagai devisa negara dan juga masuknya institusi investor. Menurut Oscar, dulu negara belum pernah sama sekali mempertimbangkan Bitcoin sebagai devisa.

    "Namun di tahun ini, negara El Salvador yang kabarnya nantinya juga akan diikuti oleh negara Amerika Selatan lainnya yang selama ini terikat dengan Dollar USD mempertimbangkan Bitcoin sebagai devisa negaranya."

    Oscar merasa di 2022, pasar sudah lebih kebal. Ini terkait dengan IMF yang cukup banyak memberikan pendapat menentang Bitcoin. Menurutnya, pendapat ini bukan sesuatu yang benar-benar bisa menggerakkan pasar.

    "Bitcoin sudah sering dinyatakan mati dari sejak kemunculannya. Saya kira statement IMF yang bertentangan dengan eksistensi kripto tidak akan begitu pengaruh," kata Oscar.

    "Yang akan cukup berpengaruh adalah bagaimana negara akan membuat bitcoin sebagai devisa atau tidak. Kita juga bisa melihat bahwa institusi juga sudah terjun dan gelombangnya cukup besar," imbuhnya.

    Oscar menyebut, jika harga turun, maka institusi akan memborong. Jika hal ini dilakukan terus menerus, lama lama supply bitcoin akan terus menipis.
    Pada bulan Januari 2021, Bitcoin berada di angka 500 juta rupiah sementara berdasarkan catatan market Indodax pada 28 Desember 2021, Bitcoin sudah menyentuh angka 737 juta.

    Bitcoin sudah naik sekitar 47.4 persen bahkan pernah menyentuh harga all time high di bulan November dengan harga hampir Rp 1 miliar per 1 Bitcoin. 

    ',
    2,
    'prediksi-ceo-indodax-soal-aset-kripto-ethereum-pada-2022',
    'eth-5.jpeg',
    'Prediksi CEO Indodax Soal Aset Kripto Ethereum pada 2022',
    false,
    now(),
    now()
)