insert into berita ("judul", "subjudul", "isiberita", "id_kategori_berita", "slug", "thumbnail", "thumb_judul", "isDraft", "createdAt", "updatedAt")
values (
	'BCA Buka Suara Soal Rumor Bikin Bursa Kripto Bareng Binance',
  	'BCA akhirnya menanggapi pemberitaan mengenai kerja sama perseroan dengan perusahaan platform bursa kripto (cryptocurrency) raksasa Binance Holdings Ltd', 
  	'Manajemen PT Bank Central Asia Tbk (BBCA) atau BCA akhirnya menanggapi pemberitaan mengenai kerja sama perseroan dengan perusahaan platform bursa kripto (cryptocurrency) raksasa Binance Holdings Ltd.
Sebelumnya, Binance dikabarkan akan mendirikan bursa kripto di Indonesia bersama bank yang dikendalikan keluarga Hartono tersebut dan emiten telekomunikasi BUMN PT Telkom Indonesia Tbk (TLKM).
Executive Vice President Secretariat & Corporate Communication BCA Hera F. Haryn mengatakan, informasi mengenai kemitraan BCA dengan Binance tidak benar.
"Sehubungan dengan adanya pemberitaan mengenai "Binance Weighs Crypto Venture With Richest Indonesian Family" yang salah satunya membahas mengenai BCA, dapat kami sampaikan bahwa Informasi tersebut tidak benar. Manajemen BCA tidak pernah mendiskusikan hal tersebut," jelas Hera F. Haryn kepada CNBC Indonesia, Minggu (12/12/2021).
Sementara, hingga artikel ini ditulis, pihak Telkom belum memberikan tanggapan mengenai rumor tersebut.
Melansir Bloomberg, Jumat (10/12), sumber anonim mengatakan, Binance sedang dalam proses diskusi dengan BCA dan Telkom terkait pendirian bursa kripto di Tanah Air.

',
  3,
  'BCA-buka-suara-soal-rumor-bikin-bursa-kripto-bareng-binance',
  'Binance1.jpeg',
  'Rumor Bikin Bursa Kripto Bareng Binance',
  false,
  now(),
  now()
),
(
	'Bos Binance Bicara Masa Depan Dogecoin, Shiba Inu & Meme Coin',
  	'Dalam beberapa tahun terakhir cryptocurrency (mata uang kripto) banyak menarik minat para investor dan trader', 
  	'Dalam beberapa tahun terakhir cryptocurrency (mata uang kripto) banyak menarik minat para investor dan trader. Harga kripto terus meningkat dan mencetak rekor tertinggi sepanjang sejarah.
Salah satu yang mendapat perhatian adalah meme coin. Ini adalah uang kripto yang awalnya hadir sebagai lelucon namun kemudian banyak investor yang tertarik. Contoh dari meme coin adalah Dogecoin, Shiba Inu hingga SQUID game.
Chief Executive Officer (CEO) Binance Changpeng Zhao dalam wawancara dengan The Assosiated Press bicara soal meme coin. Ia mengaku tidak mengerti soal Dogecoin tetapi apa yang terjadi saat ini menunjukkan kekuatan desentralisasi.
"Sejujurnya, saya tidak mengerti tentang Dogecoin. Tapi hal itu menunjukkan kekuatan desentralisasi. Apa yang saya pikirkan mungkin atau mungkin tidak penting. Jika cukup banyak orang dalam komunitas yang menghargainya karena lucu, karena mereka menyukai meme, maka meme itu memiliki nilai," ujarnya seperti dikutip Jumat (10/12/2021).
Changpeng Zhao menambahkan Dogecoin telah bertahan bertahun-tahun meski harganya berfluktuasi naik dan turun.
"Sekarang kami memiliki Shiba, yang juga merupakan koin meme. Kami memiliki lebih banyak koin meme. Tapi coba tebak? Untuk sesuatu menjadi berharga, Anda hanya perlu satu orang lain untuk mau membelinya," terangnya.
"Agar sesuatu memiliki likuiditas, Anda memerlukan sejumlah besar orang yang ingin membeli atau menjualnya. Begitu Anda memiliki likuiditas, sesuatu memiliki nilai, menurut pasar netral. Jadi bukan hak saya untuk menilainya."
Informasi saja, menurut Coinmarketcap, Dogecoin memiliki kapitalisasi pasar mencapai US$22,56 miliar. Adapun Shiba inu memiliki kapitalisasi pasar US$19,21 miliar.

    ',
    3,
    'bos-binance-bicara-masa-depan-dogecoin,-shiba-inu-meme-coin',
    'binance2.jpeg',
    'Masa Depan Dogecoin, Shiba Inu & Meme Coin',
    false,
    now(),
    now()
),
(
	'Gandeng Binance, Anak Usaha Telkom Buat Platform Kripto di RI',
  	'Perusahaan pelat merah, PT Telkom Indonesia Tbk (TLKM), bakal masuk ke bisnis perdagangan kripto melalui anak usaha MDI Ventures', 
  	'Perusahaan pelat merah, PT Telkom Indonesia Tbk (TLKM), bakal masuk ke bisnis perdagangan kripto melalui anak usaha MDI Ventures. Perusahaan modal ventura (venture capital/VC) ini akan bekerja sama dengan Binance Holdings Ltd. untuk memboyong platform trading ini masuk ke pasar dalam negeri.
CEO MDI Ventures Donald Wihardja mengatakan, MDI dan Binance sudah meneken kesepakatan kerja sama tersebut. Namun bentuk kerja sama yang akan dilakukan masih belum ditentukan menunggu regulasi mengenai kripto di dalam negeri.
"Jadi ini baru awal kerja sama, itu niatnya adalah untuk bikin crypto platform," kata Donald di Jakarta, Selasa (14/12/2021).
Dia menjelaskan, saat ini Binance masih menunggu regulasi yang jelas mengenai penyedia platfom perdagangan kripto dan regulasi mengenai kripto di Indonesia. Mengingat Binance bertekat untuk mendapatkan lisensi yang resmi untuk operasionalnya.
Kerja sama dengan Binance ini memiliki potensi yang besar. Ini mengingat Binance saat ini merupakan platform trading kripto terbesar di dunia dengan nilai transaksi mencapai US$ 49 miliar per hari.

    ',
    3,
    'gandeng-binance-anak-usaha-telkom-buat-platform-kripto-di-RI',
    'binance3.jpeg',
    'Platform Kripto di RI',
    false,
    now(),
    now()
),
(
	'Kabar Baik! Binance Mulai Diterima di Bahrain & Kanada',
  	'Binance telah mendapat lampu hijau oleh regulator di Bahrain dan Kanada untuk afiliasinya yang beroperasi di kedua negara tersebut', 
  	'Kabar baik kembali datang di pasar kripto, di mana perusahaan pertukaran kripto yakni Binance telah mendapat lampu hijau oleh regulator di Bahrain dan Kanada untuk afiliasinya yang beroperasi di kedua negara tersebut.
Binance mengumumkannya pada Senin lalu bahwa mereka telah memperoleh persetujuan prinsip dari bank sentral Bahrain untuk memantapkan dirinya sebagai penyedia layanan aset kripto di negara kepulauan itu.
Meskipun ada kepastian diterimanya Binance di Bahrain, tetapi perusahaan masih harus menyelesaikan proses aplikasi lengkap.
Jika disimpulkan, Bahrain akan menjadi negara pertama di kawasan Timur Tengah dan Afrika Utara (MENA) yang memberikan persetujuan peraturan kepada entitas Binance.
"Bank Sentral Bahrain telah menunjukkan keseriusannya dan pemikiran dalam menangani kripto sebagai kelas aset masa depan," kata Changpeng Zhao, CEO Binance, dikutip dari Forbes.
"Persetujuan tersebut mengakui komitmen Binance untuk sepenuhnya mematuhi persyaratan peraturan dan komitmen kami yang lebih luas untuk menjangkau operasi dan aktivitas di Bahrain," tambah Zhao.
Zhao juga mengumumkannya di Twitter pada hari yang sama bahwa entitas terpisah dari Binance telah terdaftar di badan intelijen keuangan Kanada, Pusat Analisis Transaksi dan Laporan Keuangan Kanada.
Entitas bernama Binance Canada Capital Markets akan menawarkan layanan yang melibatkan cryptocurrency, valuta asing, dan transfer uang.

    ',
    3,
    'kabar-baik-binance-mulai-diterima-di-Bahrain-Kanada',
    'Binance4.jpeg',
    'Binance Mulai Diterima di Bahrain & Kanada',
    false,
    now(),
    now()
),
(
	'Binance Setop Operasi di Singapura Secara Permanen, Kenapa?',
  	'Binance Holdings Ltd memutuskan untuk menarik permohonan lisensi kripto yang diajukan afiliasi lokalnya Binance Asia Services (BAS) kepada Monetary Authority of Singapore atau Bank Sentral Singapura (MAS)', 
  	'Binance Holdings Ltd memutuskan untuk menarik permohonan lisensi kripto yang diajukan afiliasi lokalnya Binance Asia Services (BAS) kepada Monetary Authority of Singapore atau Bank Sentral Singapura (MAS). Perusahaan itu juga akan menyetop operasional platform jual beli cryptocurrency.
BAS, unit Binance Singapura merupakan salah satu dari 100 lebih perusahaan cryptocurrency yang mengajukan izin untuk beroperasi di Singapura. Tidak dijelaskan alasan keputusan Binance mundur dari Singapura selain pertimbangan "strategis, komersial, dan pengembangan," tulis manajemen seperti dikutip dari Reuters, Selasa (14/12/2021).
Rencananya Binance akan setop melawarkan layanan melalui laman Binance.sg pada 13 Februari 2022. Pengguna juga harus mencairkan seluruh dana mereka pada tanggal tersebut.
Binance juga mengungkapkan pihaknya akan segera tidak menerima pengguna baru di Singapura dan tidak mengizinkan pengguna eksisting untuk menyimpan aset di platform tersebut, seperti dikutip dari Coindesk.
Seorang juru bicara Bank Sentral Singapura mengatakan Binance Asia Services telah memberi mereka rencana penghentian layanan dan "akan memberikan waktu yang cukup bagi pelanggan untuk mencari penyedia alternatif, atau, jika mereka ingin, melikuidasi kepemilikan mereka."
Sebelumnya, MAS telah memerintahkan platform pertukaran uang kripto Binance.com untuk berhenti menyediakan layanan pembayaran kepada warga Singapura pada September lalu.
Penghentian ini karena Binance.com tidak memiliki lisensi untuk melakukan layanan tersebut. Pada saat menghentikan layanan itu unit Binance lokal bernama Binance.sg sedang mengajukan aplikasi permohonan lisensi ke MAS dan selama proses pengajuan izin masih bisa menjalankan bisnis seperti biasanya.

    ',
    3,
    'binance-setop-operasi-di-Singapura-secara-permanen-kenapa',
    'binance5.jpeg',
    'Binance Setop Operasi di Singapura',
    false,
    now(),
    now()
)