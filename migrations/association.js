const { Sequelize, } = require('sequelize');
const config = require('../config');
const { berita } = require('../app/model/berita')
const { hashtag_berita } = require('../app/model/hashtag_berita')
const { hashtag } = require('../app/model/hashtag')

const db = new Sequelize(
    config.DB_NAME,
    config.DB_USERNAME,
    config.DB_PASSWORD,
    {
      host: config.DB_HOST,
      port: config.DB_PORT,
      dialect: config.DB_DIALECT,
      logging: false,
      operatorsAliases: false
    }
  );




db.sync({force: false, alter: true})
.then(function(){
  console.log('asosiasi table berhasil dibuat');
  return
})
.catch(function(err){
  console.log('berita table gagal', err)
})